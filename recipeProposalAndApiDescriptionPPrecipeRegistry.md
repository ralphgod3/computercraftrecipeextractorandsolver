## changes and additions for the recipe registry

### preface
As SirEdvin is aware I have been trying to create an AE like system using purely computercraft,  
This includes the autocrafting aspect.  
Since I was unaware the recipe registry was a thing and to be honest it might not have been when I started writing the code.
I do some processing of recipes using external scripts to create something similar as the recipe registry tries to accomplish.  
Once I was made aware the recipe registry exists i tried to use it but it has some flaws that prevent any normal (normal as in not someone with days or weeks of time to spare) person from using it.  
The changes proposed should make the recipe registry more useful whilst simultaneously hopefully making it useful for people that are less advanced programmers or do not wish to invest the amount of time required to make it useful.  
  

### tools  
As mentioned I have done quite a lot of this work already for my own ae clone whilst this clone uses computercraft and python/ C++ for the external scripts some of the tools might be useful for making design decisions.  
[git link](https://gitlab.com/ralphgod3/computercraftrecipeextractorandsolver)  
At the time of writing there is no documentation yet but the basic idea is that you put all mod jars and the forge jar in the input folder and then run jarextractor.py  
This will create a bunch of folders and files in the output folder that can be used to view how certain mods store their recipes and what is needed to convert them.  
note: My own recipe converter currently does not output recipes in the recipe format defined below since its mostly a proof of concept.  Its the first thing on my todo list when a basic version for the external scripts is fully working.  
I will try to update this with any issues I find when implementing my own ae clone.

### Current flaws or lack of functionality
1. Not all mods serialize their recipes in a to put it mildly convenient way. This leads to issues with the current implementation of the recipe registry.  
For example some minor differences like CC:tweaked using nbt tags to differentiate turtle types leads to the registry not returning a valid recipe at all.  
Some more extreme cases like ars_nouveau use a system where recipes have input items defined as named keys like item1, item2.
Other mods like Immersive engineering and Mekanism add an extra layer to getting ingredients.  
  
2. For a user it is very hard to determine what recipe type is used to produce an item unless they keep a database.  
however building such a database should not be necessary for basic use of the registry since the block would be too complicated too use for any but the most dedicated computercraft users.  
  
3. It can be hard to convert a tag returned as an ingredient in a recipe to an item with that tag.  
  
4. The final issue for now is balance. It is extremely hard to effectively use the registry with the current cooldown value.

### Current workarounds for the listed issues
1. No simple workaround the only way to get around it is to build your own recipe database and converters which most people will not be able or do not want to do.  
2. build and keep a database with known recipe types for items. should this be done than the recipe registry loses some of its attractiveness to be used at all.  
3. Since the recipe registry is intended for people making ae like systems with computercraft it can be safely assumed that there will be no very new users using it.  
This means that it is viable for a user to get all tags of of any new item entering their storage solution and using that as a lookup table for recipes.  
The main disadvantage for doing this is that it makes it much harder to handle crafts that need other crafts for their ingredients at the start of the game.  
4. attach a bunch of registries over a wired network and iterate through them with each request. This idea does sound interesting but the current 5 second time is fairy long for each lookup.    


### API changes
The current API is rather limited and has problems dealing with certain recipes like computercraft turtle upgrades.  

#### Changes to existing API
The current getRecipeFor is limited in that it will be unable to find recipes that only differ on nbt data.
A extra optional argument with nbt given as a hex string of a md5 hash (cc:tweaked returns them like this) or as just the nbt tags (ap inventory manager does this) would be needed to solve this issue.  

Arguments for the changed function:  
* itemId  
    itemId as a string ex: minecraft:oak_log
* nbt  
    optional argument given as a string.  
    when given will only try to find recipes that match itemId and nbt tag in the result of recipe.  
* type  
    recipeTypes to search in given as a table if multiple recipeTypes should be searched in, string for only 1 type or nil if all types should be returned.  
    Whilst this might seem overpowered it is extremely hard for a user to determine programmatically without using external scripts or databases what recipetype creates an item.  
    As to why this should still be an argument it is to allow the user to filter for recipe types they can create. for the mod this can speed up searching through the recipe types to speed up the function.  

Returns:
* table, nil  
    table containing recipe info (check recipe structure definition) or nil when no recipe could be found.

```lua
--current api call
table getRecipeFor(string type, string itemId)

--proposed
--when nbt is nil then only recipes that do not give an item with nbt data as a result will be returned.
table getRecipesFor(string itemId, string | nil nbt, table | string | nil type)
```


#### New functions for the API  
Some functionality to make the block easier in use should be considered.  
  
  
This is not strictly necessary and might not even fit with what this peripheral does.  
If the user wants to do sub component crafting for the recipe they actually want to create then this would be a very useful function to have.  

Arguments for function:  
* tag  
    string the tag that should be checked  

Returns:  
* table, nil  
    Table of items that have the given tag or nil if no item has the tag.  
```lua
    table GetItemsForTag(string tag)
```








### Recipe structure definition.  
#### Preface  
Whilst I obviously have a benefit into the recipe structure changing to this structure.
I genuinely believe that a standard format is needed and from my current experience with writing converters this format should be able to deal with most if not all mod recipes and should be relatively easy for the user to parse.  

#### Proposed structure
Since a lot of mods serialize recipes in interesting ways it would make sense to return a standardized format to the user.  
The format defined below should captures all information from every recipe type I have encountered thus far and should be easy enough for a user to parse and use.  
  
this recipe format is also capable of accurately describing information about recipes crafted in machines as long as the converter to produce this format is consistant.  
for example an ae2 inscriber might not have slot 1, 2, and 3 assigned the same way as the recipe format does but as long as the recipes are consistent in serialization the user can make a lookup table for this.  This is also vastly preferred too having to parse roughly 80 different formats in ~200 mods that I tested with.
```lua
recipe = {
    --the minecraft way of item and tag doesn't work with all the mods. This is because of fluids, slurries, gasses and block based crafting. so separate it into a type
    --current options I found with my recipe converter that should cover most mods:
    --item, tag, fluid, block, fluidBlock, gas, slurry, infusion and fluidTag. I am not entirely sure fluidTag is needed. This needs more testing.
    --as to why it is needed on the result or output that is because some mods like immersive engineering can have tags as a result.
    type = "item",
    --optional nbt tag on return so user can get the exact required recipe using the api. this will have to be the hash of the nbt tags like cc does it.
    nbt = {},
    --name of the produced result
    result = "minecraft:diamond_pickaxe",
    --recipes to create this item, fluid, gas, slurry
    recipes = {
        {
            --recipe type
            type = "minecraft:crafting",
            --optional count tag if more than 1 is returned
            count = 1,
            --optional tags that define crafting table or input size, only given when size is non standard crafting size (not 3x3) and not just machine inputs.
            --progressive peripherals automata crafting and mechanical crafters from create will need this
            --this could also be left to the user to figure out based on the type of craft being performed
            width = 3,
            height = 3,
            recipe = {
                --recipe is always given as a shaped recipe since converting from unshaped to shaped is easy and it eliminates complexity on user side
                [1] = {
                    type = "tag",
                    name = "forge:gems/diamond",
                    --nbt and count tag are optional since user can check for it and if there isn't a tag it means item shouldn't have nbt and count = 1
                    count = 1,
                    --nbt can be given as the actual nbt or the md5 hash as cc does it.
                    nbt = {},
                },
                [2] = {
                    type = "tag",
                    name = "forge:gems/diamond",
                },
                [3] = {
                    type = "tag",
                    name = "forge:gems/diamond",
                },
                [5] = {

                    type = "tag",
                    name = "forge:rods/wooden",
                },
                [8] = {
                    type = "tag",
                    name = "forge:rods/wooden",
                },
            },
        },
        {
            --example of a machine recipe with 1 input
            type = "thermal:smelter",
            recipe = {
                --recipe is always given as a shaped recipe since converting from unshaped to shaped is easy and it eliminates complexity on user side
                [1] = {
                    type = "tag",
                    name = "forge:gems/diamond",
                    --nbt and count tag are optional since user can check for it and if there isn't a tag it means item shouldn't have nbt and count = 1
                    count = 5
                },
            },
        },
        {
            --example of a fluid machine recipe with 1 input
            type = "thermal:refinery",
            recipe = {
                [1] = {
                    type = "fluid",
                    name = "minecraft:water",
                    --for fluids count tag is the amount of mb needed or returned as a result, i propose still naming it count or renaming count everywhere to amount to make parsing easier
                    count = 10000
                },
            },
        },
        {
            --example of a fluid machine recipe with 1 input
            type = "minecraft:crafting",
            --this recipe also returns or has the ability to return extra items
            optionalOutputs = 
            {
                {
                    --just as with the inputs and result the count and nbt tags are optional
                    type = "item",
                    name = "bloodmagic:blood_orb",
                },
                {
                    type = "item",
                    name = "minecraft:dirt",
                    -- a count that is not an integer indicates the recipe has chance to return the item but it is not guaranteed
                    --this would need some logic in the function to convert the various methods mods use to represent this but this should be the easiest for a user to handle
                    count = 0.5
                },
            },
            recipe = {
                [1] = {
                    --some mods like blood magic, mantle, tinkers construct and tetra modify some of the default assumptions made for recipes and need extra info.
                    --for these mods I propose simply serializing the extra info in the slotInfo for the recipe and having the user deal with it.
                    --there is as far as I could gather from testing no good standardized way of presenting this info to the user
                    type = "item",
                    name = "bloodmagic:blood_orb",
                    orb_tier = 3
                },
            },
        },
    },
}
```


### Closing remarks  
The things mentioned are from my own current experiences writing a recipe converter.  
The recipe registry seems like a very interesting and powerful block it is however difficult to use effectively at the moment which may lead to people not using it at all.  
