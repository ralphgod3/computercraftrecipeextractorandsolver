
## Information about this project and alternatives  
### Why does this exist?  
This exists because I have a need to get all recipes in my Minecraft modpack in a standard format.  
I need that for creating a generic autocrafting solution using only cc:tweaked.  
Should you have a need for generic computercraft autocrafting this script can be usefull for you.  
  
### Alternatives  
There is a mod called [ProgressivePeripherals](https://www.curseforge.com/minecraft/mc-mods/progressiveperipherals)  
ProgressivePeripherals has a block called the recipe registry which in essence does something similar.  
It also allows you to set custom serializers for the recipes (which you will need for a lot of machines, roughly 2/3 from my testing).  

### Why choose this over ProgressivePeripherals?  
1. At the time of writing the ProgressivePeripherals API is not yet fully documented and still changing.  
2. You play on a server and do not have the ability to add ProgressivePeripherals.  
3. All recipes have exactly the same format which allows for easier parsing.  
4. It produces a list of items that are used in the recipes for the use of creating an item database for your computercraft needs.  
  
### Why choose ProgressivePeripherals over this?
1. Recipes output will be more accurate than this script will ever be with the caveat that you write some of the serializers yourself.  
This is due to a limitation in grabbing the recipes from the jar files which I do not think can be solved.  
2.  Easier to setup and use.  
  
I recommend trying to use ProgressivePeripherals if you can since it should be more accurate and is easier to use.  
## Instructions and output formats  
### Instructions  
1. Install python 3 if you do not have it yet.  
2. clone/download the repo.  
3. In the input folder input the mod jars you wish to get the recipes for.  
4. Repeat step 3 for the config files located in (instanceName)/config.  
5. Repeat step 3 for world config files located in saves/(worldName)/serverconfig.  
6. Open a terminal and run the recipeExtractor script located in jarextractor folder.  
7. Inspect logs located in output/logs.txt and add anything it listed as skipping for a reason into a toml file (check standardAditions.toml for an example).  
8. Run the script again this time passing your own toml file location as a command line argument to the script or changing the standardAditions.toml.  
9. Repeat step 7 and 8 until statisfied the script did not miss any items or mods you care about.  
10. Locate the items.json and recipes.json in the output folder and use these for what you need.  
  
### Item output definition  
Items are output in a json array where each entry is a json object.  
Every item object has a name and an optional nbtUnhashed key.  
```json
[    
    {
        "name": "advancedperipherals:rs_bridge"
    },
    {
        "name": "refinedstorage:quartz_enriched_iron"
    },
    {
        "name": "refinedstorage:interface"
    },
    {
        "name": "minecraft:potion",
        "nbtUnhashed": "{Potion:\"minecraft:long_regeneration\"}"
    }
]
```
#### Recipe output structure  
The recipe structure is created to allow for easy parsing.  
Each item that is input or output from a recipe has a name, type and optional nbt and count field.  
Every recipe is returned as a shaped recipe where the numbers corespond to the slot the item should be in to get crafted.  
For machines the numbers are chosen from 1 to n but all machine recipes are serialized the same for the same machine.  
This should allow for the creation of a lookup table should you want to use CC to inject items directly into the machine.  
  
example of a recipe with nbt data, a recipe with optional outputs and an item with multiple methods of creation:  
```json
[
    {
        "name": "computercraft:turtle_normal",
        "nbtUnhashed": "{RightUpgrade:\"computercraft:wireless_modem_advanced\"}",
        "type": "item",
        "recipes":
        [
            {
                "count": 1,
                "recipe": {
                    "1": {
                        "type": "item",
                        "name": "computercraft:turtle_normal",
                        "count": 1
                    },
                    "2": {
                        "type": "item",
                        "name": "computercraft:wireless_modem_advanced",
                        "count": 1
                    }
                },
                "type": "minecraft:crafting"
            }
        ]
    },
    {
        "name": "thermal:niter",
        "type": "item",
        "recipes": [
            {
                "count": 2,
                "optional": [
                    {
                        "type": "item",
                        "name": "thermal:niter",
                        "count": 1
                    },
                    {
                        "type": "item",
                        "name": "minecraft:cobblestone",
                        "count": 1
                    }
                ],
                "recipe": {
                    "1": {
                        "type": "tag",
                        "name": "forge:ores/niter",
                        "count": 1
                    }
                },
                "type": "create:crushing"
            },
            {
                "count": 6,
                "recipe": {
                    "1": {
                        "type": "tag",
                        "name": "forge:ores/niter",
                        "count": 1
                    }
                },
                "type": "thermal:pulverizer"
            },
            {
                "count": 3,
                "optional": [
                    {
                        "type": "item",
                        "name": "thermal:rich_slag",
                        "count": 1
                    }
                ],
                "recipe": {
                    "1": {
                        "type": "tag",
                        "name": "forge:ores/niter",
                        "count": 1
                    }
                },
                "type": "thermal:smelter"
            },
            {
                "count": 1,
                "recipe": {
                    "1": {
                        "type": "item",
                        "name": "thermal:niter_ore",
                        "count": 1
                    }
                },
                "type": "minecraft:blasting"
            },
            {
                "count": 1,
                "recipe": {
                    "1": {
                        "type": "item",
                        "name": "thermal:niter_ore",
                        "count": 1
                    }
                },
                "type": "minecraft:smelting"
            },
            {
                "count": 9,
                "recipe": {
                    "1": {
                        "type": "item",
                        "name": "thermal:niter_block",
                        "count": 1
                    }
                },
                "type": "minecraft:crafting"
            }
        ]
    }
]
```

## Frequently asked questions  
1. Why does your code suck?  
A: This is a hobby project and I do not know Python, feel free to submit a pull request if something is bothering you.  
1. Why is mod X not giving any output?  
A: I probably do not use that mod and, you can try creating an issue and i might add support but honestly it is far faster if you add support yourself and add a pull request.  
1. Why are some recipes from mod X not giving any output?  
A: Writing converters for every type of recipe is very time intensive since every handler has to be written just for that recipe.  
I dislike taking a generic approach and writing specialized ones for stuff that breaks, If i output a recipe I am sure it will work.  
Additionally not all recipes are available in the jar file and there is not really anything i can do about that.  
1. Why did you not just create a mod with the sole purpose of doing exactly this?  
A: I am asking myself the same question after spending a week writing all these converters.  
Initially I did not do this because I have no Java knowledge and i felt like it would be easier doing it like this, I am not so sure about that after creating this.  
For now I am helping sirEdvin where I can by providing feedback and possible solutions to some flaws of the recipe registry so it can replace this script in the future.  
1. Why is it so slow?  
A: The nature of this script requires it to only run when mods are added or removed to a modpack thus no effort was made to optimize for speed.  
Should you want to improve the speed yourself I would suggest adding caching of output items that are already in the itemlist.  
1. Can I have the computercraft autocrafter please?  
  A: It is heavily integrated with my clone of AE using computercraft. This means you will have to run my entire ae clone for it to be usefull.  
  The autocrafting aspect is currently being rewritten to use these recipe files, it is not fully operational at the moment.  
  If you do not care about this then you can find it [here](https://gitlab.com/ralphgod3/computercraft) it is located in storageSystem/craftingController.  
1. Will You be providing a recipe solver for computercraft?  
A: Maybe, currently I am rewriting some stuff to allow my recipe format and the ProgressivePeripherals recipe registry to be used for my autocrafter.  
I do have plans for writing a recipe solver in C++ and then using websockets to connect it with computercraft which should alleviate any performance issues.  
1. Why dont you output the same format as the recipe registry?  
A: Our approaches both have limitations which is the reason for the differences in output format.  
## Information for contributors 
There is no posiblity of me adding every mod there is so if you would like to add a mod or improve serialization for an existing mod create a pull request.  
To help with development, the script also outputs some intermediary files to the output folder.  
These files can be used as an aid for writing new serializers.  
If you wish to improve existing serializers or add new ones take a look at recipesByType which outputs the files split into crafting handler type as they come from the jar files.    
If you wish to improve the conditional system check logs.txt.  
If you wish to improve the reading of tags from the jar files check ItemsAndTagsFromTagFiles folder.  