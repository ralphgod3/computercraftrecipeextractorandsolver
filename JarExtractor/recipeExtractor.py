from libraries import ItemDb
from libraries import utils
from libraries import recipeConditionalUtils
import sys
import os
import json
import shutil
from libraries import converter

# minecraft jar file not nescesary, forge file contains a copy with all recipes from minecraft file

locationForJarFiles = "input\\jars"
locationForConfigFiles = "input\\config"
locationForServerConfigFiles = "input\\serverconfig"

outputLocation = "output"
locationForAllRecipesFile = outputLocation + "\\recipes"
locationForInterMediateCraftingHandlers = outputLocation + "\\recipesByType"
locationForItemAndTagDb = outputLocation + "\\ItemsAndTagsFromTagFiles"
logLocation = outputLocation + "\\logs.txt"
convertedRecipesOutputLocation = outputLocation + "\\convertedRecipes.json"
outputItemLocation = outputLocation + "\\items.json"


def GetRecipesFromInputFolder(cwd, files):
    recipes = []
    for file in files:
        if ".jar" in file:
            tempRecipes = utils.GetJarRecipes(
                cwd + "\\" + locationForJarFiles + "\\" + file)
            for recipe in tempRecipes:
                recipes.append(recipe)
    return recipes


def GetTagsFromInputFolder(cwd, files):
    db = ItemDb.itemAndTagDb()
    for file in files:
        if ".jar" in file:
            tempItemData = utils.GetItemDataFromJar(
                cwd + "\\" + locationForJarFiles + "\\" + file)
            for data in tempItemData:
                db.AddItemsToDb(data["tagName"], data["items"])
    return db


utils.Print("put jar files in \\" + locationForJarFiles)
utils.Print("put config files in \\" + locationForConfigFiles)
utils.Print("put serverconfig files in \\" + locationForServerConfigFiles)
cwd = os.path.dirname(os.path.realpath(__file__))


nonStandardConditionalsToEvaluate = []
aditionalitems = []
aditonalArgs = "standardAditionFile.toml"
# check if user provided an override for the argument file
if len(sys.argv) > 1:
    aditonalArgs = sys.argv[1]
try:
    args = utils.GetTomlData(cwd + "\\" + aditonalArgs)
    if "items" not in args or "tags" not in args or "others" not in args:
        print("missing items, tags or others array in adition file")
        exit()
    for item in args["items"]:
        aditionalitems.append(item)
        nonStandardConditionalsToEvaluate.append(item)
    for tag in args["tags"]:
        nonStandardConditionalsToEvaluate.append(tag)
    for other in args["others"]:
        nonStandardConditionalsToEvaluate.append(other)
except:
    print("could not open " + cwd + "\\" + aditonalArgs)
    exit()


# remove old output data
utils.Print("removing old output data")
files = os.listdir(cwd + "\\" + outputLocation)
for file in files:
    if os.path.isdir(cwd + "\\" + outputLocation + "\\" + file):
        shutil.rmtree(cwd + "\\" + outputLocation + "\\" + file)

# get recipe data
utils.Print("loading recipe data")
files = os.listdir(cwd + "\\" + locationForJarFiles)
recipes = GetRecipesFromInputFolder(cwd, files)
splitByTypes = utils.SplitRecipesByType(recipes)

utils.Print("saving recipes in " + locationForAllRecipesFile)
utils.SaveJsonFile(cwd + "\\" + locationForAllRecipesFile + ".json", recipes)


# save crafting recipes by type
utils.Print("saving recipes split by crafting handler in " +
            cwd + "\\" + locationForInterMediateCraftingHandlers)
for Type in splitByTypes:
    utils.SaveJsonFile(cwd+"\\"+locationForInterMediateCraftingHandlers+"\\" +
                       Type.replace(":", "_").replace("/", "_") + ".json", splitByTypes[Type])

utils.Print("loading items and tags")
# get item and tags from tag files
itemDb = GetTagsFromInputFolder(cwd, files)
utils.Print("saving items and tags in " + cwd + "\\" + locationForItemAndTagDb)
# save output for verification later
tags = itemDb.GetTagsInDb()
utils.SaveJsonFile(cwd + "\\" + locationForItemAndTagDb + "\\tags.json", tags)
items = itemDb.GetItemsInDb()
utils.SaveJsonFile(cwd + "\\" + locationForItemAndTagDb +
                   "\\items.json", items)
toConvert, _ = itemDb.GetDb()
utils.SaveJsonFile(cwd + "\\" + locationForItemAndTagDb +
                   "\\tagsAndItems.json", toConvert)

# get config data
utils.Print("reading configuration files")
configFiles = utils.ListFilesRecursively(cwd + "\\" + locationForConfigFiles)
tempConfigs = utils.ListFilesRecursively(
    cwd + "\\" + locationForServerConfigFiles)
for file in tempConfigs:
    configFiles.append(file)
configs = {}
for filePath in configFiles:
    if utils.IsTomlFile(filePath):
        fp = filePath.split("\\")
        fp = fp[-1]
        name = fp.replace(".toml", "")
        if "client" in name:
            continue
        name = name.replace("-client", "")
        name = name.replace("-common", "")
        configs[name] = utils.GetTomlData(filePath)

utils.Print("filtering recipes based on config")
# create list of loaded mods
modList = []
for recipe in recipes:
    if recipe["jarName"] not in modList:
        modList.append(recipe["jarName"])


# go through all recipes
filteredRecipes = []
printedSkipWarnings = []
totalRecipes = len(recipes)
for recipe in recipes:
    # if recipe has a condition then solve the condition if it can be solved add the recipe
    if recipeConditionalUtils.RecipeHasConditional(recipe["recipe"]):
        addRecipe = True
        for condition in recipe["recipe"]["conditions"]:
            if not recipeConditionalUtils.SolveConditional(condition, nonStandardConditionalsToEvaluate, items, tags, modList, configs):
                text = condition
                if type(condition) is dict:
                    text = json.dumps(condition, indent=3)
                # only show each reason for skipping a recipe once
                if text not in printedSkipWarnings:
                    printedSkipWarnings.append(text)
                    utils.Print(text)
                addRecipe = False
                break
        if addRecipe:
            entry = {}
            entry["mod"] = recipe["mod"]
            entry["jarName"] = recipe["jarName"]
            entry["recipe"] = recipe["recipe"]
            if "recipe" in recipe["recipe"]:
                entry["recipe"] = recipe["recipe"]["recipe"]
            filteredRecipes.append(entry)
    # if the recipe is a conditional then solve that
    elif "type" in recipe["recipe"] and recipe["recipe"]["type"] == "forge:conditional":
        for r in recipe["recipe"]["recipes"]:
            addRecipe = True
            for condition in r["conditions"]:
                if not recipeConditionalUtils.SolveConditional(condition, nonStandardConditionalsToEvaluate, items, tags, modList, configs):
                    text = condition
                    if type(condition) is dict:
                        text = json.dumps(condition, indent=3)
                    # only show each reason for skipping a recipe once
                    if text not in printedSkipWarnings:
                        printedSkipWarnings.append(text)
                        print("-----")
                        utils.Print(text)
                        print("n")
                    addRecipe = False
                    break
            if addRecipe:
                entry = {}
                entry["mod"] = recipe["mod"]
                entry["jarName"] = recipe["jarName"]
                entry["recipe"] = r
                recipes.append(entry)
    else:
        filteredRecipes.append(recipe)


recipes = []
utils.Print("compressing recipe data for easier parsing in lua")
# start converting recipes
for recipe in filteredRecipes:
    if "type" in recipe["recipe"]:
        ret = converter.ConvertRecipe(recipe["recipe"])
        if ret and "name" in ret:
            print("adding recipe for " + ret["name"])
            newRecipe = True
            for r in recipes:
                if r["name"] == ret["name"]:
                    if "nbtUnhashed" in ret and "nbtUnhashed" in r and ret["nbtUnhashed"] == r["nbtUnhashed"]:
                        for rr in ret["recipes"]:
                            r["recipes"].append(rr)
                        newRecipe = False
                        break
                    elif "nbtUnhashed" not in ret and "nbtUnhashed" not in r:
                        for rr in ret["recipes"]:
                            r["recipes"].append(rr)
                        newRecipe = False
                        break
            if newRecipe:
                recipes.append(ret)
    else:
        text = "skipping recipe without type: " + json.dumps(recipe, indent=3)
        if text not in printedSkipWarnings:
            printedSkipWarnings.append(text)
            utils.Print(text)


file = open(cwd + "\\" + convertedRecipesOutputLocation, "w")
file.write(json.dumps(recipes))
file.close()

outItems = []

utils.Print("generating item list")
for item in items:
    entry = {}
    entry["name"] = item
    outItems.append(entry)
for item in aditionalitems:
    entry = {}
    entry["type"] = "item"
    entry["name"] = item
    if utils.ItemNeedsToBeAddedToItems(outItems, entry):
        outItems.append(utils.CreateItemEntryForItem(entry))
for item in recipes:
    # add result to list if needed
    if utils.ItemNeedsToBeAddedToItems(outItems, item):
        outItems.append(utils.CreateItemEntryForItem(item))
    for recipe in item["recipes"]:
        if "optional" in recipe:
            for optItem in recipe["optional"]:
                if utils.ItemNeedsToBeAddedToItems(outItems, optItem):
                    outItems.append(utils.CreateItemEntryForItem(optItem))
        for input in recipe["recipe"]:
            if utils.ItemNeedsToBeAddedToItems(outItems, recipe["recipe"][input]):
                outItems.append(utils.CreateItemEntryForItem(
                    recipe["recipe"][input]))

utils.Print("recipes before conditional solving " + str(totalRecipes))
utils.Print("recipes after conditional solving " + str(len(filteredRecipes)))
utils.Print(
    "recipes after cleaning, compressing and standardization " + str(len(recipes)))
utils.Print("converted recipes saved in " + convertedRecipesOutputLocation)
utils.Print("item list contains " + str(len(items)) + " items")
utils.Print("saving item list in " + outputItemLocation)
file = open(cwd + "\\" + outputItemLocation, "w")
file.writelines(json.dumps(outItems))
file.close

utils.Print("logs saved in \\" + logLocation)
file = open(cwd + "\\" + logLocation, "w")
file.writelines(utils.getLogs())
file.flush()
file.close
utils.Print("converter done")
