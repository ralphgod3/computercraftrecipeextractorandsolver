def __GetConditionalType(recipeConditional):
    if ("type" not in recipeConditional and "values" in recipeConditional) or recipeConditional["type"] == "forge:and":
        return "forge:and"
    return recipeConditional["type"]

def __SolveForgeModLoadedConditional(recipeConditional, loadedMods):    
    for mod in loadedMods:
        if recipeConditional["modid"].lower() in mod.lower():
            return True
    return False

def __SolveForgeTagEmptyConditional(recipeConditional, tags, nonStandardConditionalsToEvaluate):
    for tag in tags:
        if recipeConditional["tag"].lower() in tag.lower():
            return False
    if recipeConditional["tag"] in nonStandardConditionalsToEvaluate:
        return False
    return True

def __SolveForgeItemExistsConditional(recipeConditional, items, nonStandardConditionalsToEvaluate):
    if recipeConditional["item"] in items:
        return True
    if recipeConditional["item"] in nonStandardConditionalsToEvaluate:
        return True
    return False

def __SolveForgeAndConditional(recipeConditional, nonStandardConditionalsToEvaluate, items, tags, loadedMods, configs):
    for condition in recipeConditional["values"]:
        if type(condition) is dict:
            if not SolveConditional(condition, nonStandardConditionalsToEvaluate, items, tags, loadedMods, configs):
                return False
        else:
            if condition not in nonStandardConditionalsToEvaluate:
                print("non standard and condition: " + condition)
                return False
    return True

def __SolveForgeNotConditional(recipeConditional, nonStandardConditonalsToEvaluate, items, tags, loadedMods, configs):
    if type(recipeConditional["value"]) is dict:
        return not SolveConditional(recipeConditional["value"], nonStandardConditonalsToEvaluate, items, tags, loadedMods, configs)
    elif recipeConditional["value"] in nonStandardConditonalsToEvaluate:
        return False
    print("not condition: " + recipeConditional)
    return True

def __SolvebotaniaFluxfieldEnabledConditional(recipeConditional, nonStandardConditionalsToEvaluate, config):
    if config["botania"]["manaFluxfield"] == True:
        return True
    return False

def __SolveCofhCoreFlagConditional(recipeConditional, nonStandardConditionalsToEvaluate, config):
    if recipeConditional["flag"] in nonStandardConditionalsToEvaluate:
        return True
    return False

def __SolveImmersivepostsCfgConditional(recipeConditional, nonStandardConditionalsToEvaluate, config):
    return config["immersiveposts"][recipeConditional["key"]]

def __SolveMekanismModVersionConditional(recipeConditional, nonStandardConditionalsToEvaluate, loadedMods, config):
    for mod in loadedMods:
        if recipeConditional["modid"].lower() in mod.lower():
            print("update __solveMekanismModVersionConditional in recipeConditionalUtils.py")
    return False

def __SolveNaturesAuraEnabledConditional(recipeConditional, nonStandardConditionalsToEvaluate, config):
    return config["naturesaura"]["features"][recipeConditional["config"]]

def __SolveRsGaugesOptionalConditional(recipeConditional, items, nonStandardConditionalsToEvaluate, config):
    #FUCK RS GAUGES nobody wants their shit anyway
    #they didnt register any item and check if every single item exists in their mod meaning you have to add all of them to nonStandardConditionals for it to work
    return True

def __SolveSimplyJetpacksModIntegrationConditional(recipeConditional, nonStandardConditionalsToEvaluate, loadedMods, config):
    for mod in loadedMods:
        if recipeConditional["modid"] in mod:
            return True
    return True

def __SolveTConstructConfigConditional(recipeConditional, nonStandardConditionalsToEvaluate, config):
    if recipeConditional["prop"] == "wither_bone_conversion":
        return config["tconstruct"]["recipes"]["witherBoneConversion"]
    elif recipeConditional["prop"] == "gravel_to_flint":
        return config["tconstruct"]["recipes"]["addGravelToFlintRecipe"]
    return False

def __SolveQuarkFlagConditional(recipeConditional, nonStandardConditionalsToEvaluate, config):
    #todo: figure out what quark does
    return False

def __SolveThermalFlagConditional(recipeConditional, nonStandardConditionalsToEvaluate, config):
    #same problem as rs gauges except we might actually want these machines
    return True

def __SolveTitaniumContentExistsConditional(recipeConditional, nonStandardConditionalsToEvaluate, config):
    if "name" in recipeConditional and recipeConditional["name"] == "titanium:test_serializer":
        return False
    return True

def SolveConditional(recipeConditional, nonStandardConditionalsToEvaluate, items, tags, loadedMods, configs):
    """solve conditional in a recipe
    Args:
        recipeConditional (dict): recipe["conditions"]
        nonStandardConditionalsToEvaluate (list): list of non standard conditionals to solve to true
        items (list): items in the modpack
        tags (list): tags in the modpack
        loadedMods (list): jar file names of mods in modpack
        configs (list): config files as python dictionary

    Returns:
        boolean: true if recipe should be converted, false if not
    """
    if type(recipeConditional) is not dict:
        print("special mod detected skipping recipe")
        return False
    conditionalType = __GetConditionalType(recipeConditional)
    if conditionalType == "forge:and":
        return __SolveForgeAndConditional(recipeConditional, nonStandardConditionalsToEvaluate, items, tags, loadedMods, configs)
    elif conditionalType == "forge:not":
        return __SolveForgeNotConditional(recipeConditional, nonStandardConditionalsToEvaluate, items ,tags, loadedMods, configs)
    elif conditionalType == "forge:tag_empty":
        return __SolveForgeTagEmptyConditional(recipeConditional, tags, nonStandardConditionalsToEvaluate)
    elif conditionalType == "forge:item_exists":
        return __SolveForgeItemExistsConditional(recipeConditional, items, nonStandardConditionalsToEvaluate)
    elif conditionalType == "forge:mod_loaded":
        return __SolveForgeModLoadedConditional(recipeConditional, loadedMods)
    elif conditionalType == "botania:fluxfield_enabled":
        return __SolvebotaniaFluxfieldEnabledConditional(recipeConditional, nonStandardConditionalsToEvaluate, configs)
    elif conditionalType == "cofh_core:flag":
        return __SolveCofhCoreFlagConditional(recipeConditional, nonStandardConditionalsToEvaluate, configs)
    elif conditionalType == "immersiveposts:cfg":
        return __SolveImmersivepostsCfgConditional(recipeConditional, nonStandardConditionalsToEvaluate, configs)
    elif conditionalType == "mekanism:mod_version_loaded":
        return __SolveMekanismModVersionConditional(recipeConditional, nonStandardConditionalsToEvaluate, loadedMods, configs)
    elif conditionalType == "naturesaura:enabled":
        return __SolveNaturesAuraEnabledConditional(recipeConditional, nonStandardConditionalsToEvaluate, configs)
    elif conditionalType == "rsgauges:optional":
        return __SolveRsGaugesOptionalConditional(recipeConditional, nonStandardConditionalsToEvaluate, items, configs)
    elif conditionalType == "simplyjetpacks:mod_integration":
        return __SolveSimplyJetpacksModIntegrationConditional(recipeConditional, nonStandardConditionalsToEvaluate, loadedMods, configs)
    elif conditionalType == "tconstruct:config":
        return __SolveTConstructConfigConditional(recipeConditional, nonStandardConditionalsToEvaluate, configs)
    elif conditionalType == "quark:flag":
        return __SolveQuarkFlagConditional(recipeConditional, nonStandardConditionalsToEvaluate, configs)
    elif conditionalType == "thermal:flag":
        return __SolveThermalFlagConditional(recipeConditional, nonStandardConditionalsToEvaluate, configs)
    elif conditionalType == "titanium:content_exists":
        return __SolveTitaniumContentExistsConditional(recipeConditional, nonStandardConditionalsToEvaluate, configs)
    elif conditionalType in nonStandardConditionalsToEvaluate:
        return True
    print("unknown conditional found "  + conditionalType)
    return False

def RecipeHasConditional(recipe):
    """checks if recipe has conditional

    Args:
        recipe (dict): recipe field of loaded recipes in json

    Returns:
        boolean: true if recipe has conditional, false if not 
    """
    if "conditions" in recipe:
        return True
    return False
