import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils




def __ConvertBuildingGadgetsConstructionPasteRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def ConvertBuildingGadgetsRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "buildinggadgets:construction_paste":
        return __ConvertBuildingGadgetsConstructionPasteRecipe(recipeToConvert, Type)
    
    utils.Print("no recipe converter for " + Type)
    return None




#used for testing conversions
#testRecipeFileName = "buildinggadgets_construction_paste"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertBuildingGadgetsRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()