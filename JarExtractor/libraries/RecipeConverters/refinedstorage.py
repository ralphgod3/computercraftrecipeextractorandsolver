import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def ConvertRefinedStorageRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    typesToIgnore = [
        "refinedstorage",
        "refinedstorage:upgrade_with_enchanted_book",
        "refinedstorage:cover_recipe",
        "refinedstorage:hollow_cover_recipe",
    ]
    for t in typesToIgnore:
        if t == Type:
            return None
    utils.Print("no recipe converter for " + Type)
    return None
