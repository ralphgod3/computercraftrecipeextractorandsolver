import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertBotaniaArmorUpgradeRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(recipeToConvert)
    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(converterUtils.craftingHandler)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()

def __ConvertBotaniaElvenTradeRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(recipeToConvert)
    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    if type(recipeToConvert["output"]) is list:
        outRecipe.SetOutputNameAndType(recipeToConvert["output"][0])
    else:
        outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    return outRecipe.PrepareForSerialization()

def __ConvertBotaniaGogAlternationRecipe(recipeToConvert, recipeType):
    recipe = None
    if "key" in recipeToConvert:
        recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(recipeToConvert)
    else:
        recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(recipeToConvert)
    #consistency is hard for mod devs even in the SAME FUCKING FILE
    outKey = "result"
    if outKey not in recipeToConvert:
        outKey = "output"

    _,_,outCount,_ = converterUtils.convertItemToInfo(recipeToConvert[outKey])
    recipe.SetType(converterUtils.craftingHandler)
    recipe.SetOutputCount(outCount)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert[outKey])
    return outRecipe.PrepareForSerialization()

def __ConvertBotaniaManaInfusionRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    if type(recipeToConvert["input"]) is list:
        #todo: handle recipes with alternatives
        recipe.AddSlotInRecipe(1, recipeToConvert["input"][0])
    else:
        recipe.AddSlotInRecipe(1, recipeToConvert["input"])
    _,_,outCount,_ = converterUtils.convertItemToInfo(recipeToConvert["output"])
    recipe.SetOutputCount(outCount)
    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    return outRecipe.PrepareForSerialization()

def __ConvertBotaniaManaUpgradeRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()

def __ConvertBotaniaPetalApothecaryRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(recipeToConvert)
    recipe.SetType(recipeType)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    return outRecipe.PrepareForSerialization()

def __ConvertBotaniaPureDaisyRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    recipe.AddSlotInRecipe(1, recipeToConvert["input"])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    return outRecipe.PrepareForSerialization()

def __ConvertBotaniaRunicAltarRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(recipeToConvert)
    _,_,outCount,_ = converterUtils.convertItemToInfo(recipeToConvert["output"])
    recipe.SetType(recipeType)
    recipe.SetOutputCount(outCount)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    return outRecipe.PrepareForSerialization()

def __ConvertBotaniaTerraPlateRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(recipeToConvert)
    _,_,outCount,_ = converterUtils.convertItemToInfo(recipeToConvert["result"])
    recipe.SetType(recipeType)
    recipe.SetOutputCount(outCount)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()

def __ConvertBotaniaTwigWandRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def ConvertBotaniaRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "botania_ancient_will_attach":
        return None
    elif Type == "botania:armor_upgrade":
        return __ConvertBotaniaArmorUpgradeRecipe(recipeToConvert, Type)
    elif Type == "botania:banner_pattern_apply":
        return None
    elif Type == "botania:black_hole_talisman_extract":
        return None
    elif Type == "botania:brew": # maybe add later
        return None
    elif Type == "botania:composite_lens":
        return None
    elif Type == "botania:cosmetic_attach":
        return None
    elif Type == "botania:cosmetic_remove":
        return None
    elif Type == "botania:elven_trade":
        return __ConvertBotaniaElvenTradeRecipe(recipeToConvert, Type)
    elif Type == "botania:elven_trade_lexicon":
        return None
    elif Type == "botania:gog_alternation": #garden of glass recipe alternatives arent converted atm
        return None #__ConvertBotaniaGogAlternationRecipe(recipeToConvert["base"])
    elif Type == "botania:keep_ivy":
        return None
    elif Type == "botania:laputa_shard_upgrade":
        return None
    elif Type == "botania:lens_dye":
        return None
    elif Type == "botania:mana_gun_add_clip":
        return None
    elif Type == "botania:mana_gun_add_lens":
        return None
    elif Type == "botania:mana_gun_remove_lens":
        return None
    elif Type == "botania:mana_infusion":
        return __ConvertBotaniaManaInfusionRecipe(recipeToConvert, Type)
    elif Type == "botania:mana_upgrade":
        return __ConvertBotaniaManaUpgradeRecipe(recipeToConvert, Type)
    elif Type == "botania:mana_upgrade_shapeless":
        return None
    elif Type == "botania:petal_apothecary":
        return __ConvertBotaniaPetalApothecaryRecipe(recipeToConvert, Type)
    elif Type == "botania:phantom_ink_apply":
        return None
    elif Type == "botania:pure_daisy":
        return __ConvertBotaniaPureDaisyRecipe(recipeToConvert, Type)
    elif Type == "botania:runic_altar":
        return __ConvertBotaniaRunicAltarRecipe(recipeToConvert, Type)
    elif Type == "botania:runic_altar_head":
        return None
    elif Type == "botania:spell_cloth_apply":
        return None
    elif Type == "botania:split_lens":
        return None
    elif Type == "botania:terra_pick_tipping":
        return None
    elif Type == "botania:terra_plate":
        return __ConvertBotaniaTerraPlateRecipe(recipeToConvert, Type)
    elif Type == "botania:twig_wand":
        return __ConvertBotaniaTwigWandRecipe(recipeToConvert, Type)
    
    utils.Print("no recipe converter for " + Type)
    return None




#used for testing conversions
#testRecipeFileName = "botania_gog_alternation"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertBotaniaRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()