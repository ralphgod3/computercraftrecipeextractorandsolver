import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def __ConvertCyclicMelterRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    if "inputFirst" in recipeToConvert:
        recipe.AddSlotInRecipe(1, recipeToConvert["inputFirst"])
    if "inputSecond" in recipeToConvert:
        recipe.AddSlotInRecipe(2, recipeToConvert["inputSecond"])
    _,_,count,_ = converterUtils.convertItemToInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()

def __ConvertCyclicSolidifierRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    if "inputTop" in recipeToConvert:
        recipe.AddSlotInRecipe(1, recipeToConvert["inputTop"])
    if "inputMiddle" in recipeToConvert:
        recipe.AddSlotInRecipe(2, recipeToConvert["inputMiddle"])
    if "inputBottom" in recipeToConvert:
        recipe.AddSlotInRecipe(3, recipeToConvert["inputBottom"])
    if "mix" in recipeToConvert:
        recipe.AddSlotInRecipe(4, recipeToConvert["mix"])
    _,_,count,_ = converterUtils.convertItemToInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()

def ConvertCyclicRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "cyclic:melter":
        return __ConvertCyclicMelterRecipe(recipeToConvert, Type)
    elif Type == "cyclic:solidifier":
        return __ConvertCyclicSolidifierRecipe(recipeToConvert, Type)
    
    utils.Print("no recipe converter for " + Type)
    return None




##used for testing conversions
#testRecipeFileName = "cyclic_solidifier"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertCyclicRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()