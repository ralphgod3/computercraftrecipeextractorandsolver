import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def __ConvertFluxNetworksFluxStorageRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    _,_,count,_ = converterUtils.convertItemToInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()

def ConvertFluxNetworksRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "fluxnetworks:flux_storage_recipe":
        return __ConvertFluxNetworksFluxStorageRecipe(recipeToConvert, Type)
    elif Type == "fluxnetworks:nbt_wipe_recipe":
        return None
    
    utils.Print("no recipe converter for " + Type)
    return None




##used for testing conversions
#testRecipeFileName = "fluxnetworks_flux_storage_recipe"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertFluxNetworksRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()