import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def ConvertEmojifulRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "emojiful:emoji_recipe":
        return None
    utils.Print("no recipe converter for " + Type)
    return None