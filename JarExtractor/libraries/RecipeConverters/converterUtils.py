from typing import Union
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

# standard crafting handler we output when recipe can be made in normal crafting table
craftingHandler = "minecraft:crafting"


class ItemRecipeRecipe():
    """helper class to create 1 recipe for an item recipe(great name i know)"""

    def __init__(self):
        self.__recipe: dict[str, dict[str, any]] = {}
        self.__optionalOutputs: list[dict[str, any]] = []
        self.__outCount: int = 1
        self.__type: str = "minecraft:crafting"
        self.__width: Union[int, None] = None
        self.__height: Union[int, None] = None

    def AddSlotInRecipeManually(self, index: Union[str, int], itemType: str, itemName: str, itemCount: int = 1, nbt: Union[str, None] = None):
        """add a item with manual data extraction from mc recipe format

        Args:
            index (string | number): slot for item
            itemType (string): item type string currently only item, tag, fluid are supported
            itemName (string): itemID
            itemCount (int, optional): item count. Defaults to 1.
            nbt (string, optional): optional nbt hash. Defaults to None.
        Raises:
            NotImplementedError: when itemType is not supported
        """
        if itemType != "item" and itemType != "tag" and itemType != "fluid" and itemType != "block" and itemType != "fluidTag":
            raise NotImplementedError("itemtype not supported" + itemType)

        slotInfo = {}
        slotInfo["type"] = itemType
        slotInfo["name"] = FixItemOrTagName(itemName)
        slotInfo["count"] = itemCount
        if nbt:
            slotInfo["nbtUnhashed"] = nbt

        self.__recipe[str(index)] = slotInfo

    def AddSlotInRecipe(self, index: Union[str, int], itemInfo: dict[str, any], nbtTag: Union[str, None] = "nbt"):
        """adds an item in a specific slot to recipe

        Args:
            index (number | string): index for the item to be inserted for crafting
            itemInfo (itemInfo for item to add): itemInfo dict containing item | tag, count for item 
            nbt (string, optional): nbt tag name. Defaults to nbt.
        """
        slotInfo = {}
        slotInfo["type"], slotInfo["name"], slotInfo["count"], nbt = convertItemToInfo(
            itemInfo, nbtTag)
        if nbt:
            slotInfo["nbtUnhashed"] = nbt

        self.__recipe[str(index)] = slotInfo

    def AddOptionalOutput(self, optionalOutput: dict[str, any], nbtTag: Union[str, None] = "nbt"):
        """add additional output item to recipe

        Args:
            optionalOutput (dic containing, item | tag, count field): item information for optional item output
            nbtTag (string, optional) nbt tag name for item. defaults to "nbt"
        """
        type, name, count, nbt = convertItemToInfo(optionalOutput, nbtTag)
        tempOpt = {}
        tempOpt["type"] = type
        tempOpt["name"] = name
        tempOpt["count"] = count
        if nbt:
            tempOpt["nbtUnhashed"] = nbt
        self.__optionalOutputs.append(tempOpt)

    def AddOptionalOutputManually(self, itemType: str, itemName: str, itemCount: int = 1, nbt: Union[str, None] = None):
        """add a additional output item with manual data extraction from mc recipe format
        Args:
            itemType (string): item type string currently only item, tag, fluid are supported
            itemName (string): itemID
            itemCount (int, optional): item count. Defaults to 1.
            nbt (string, optional): optional nbt hash. Defaults to None.
        Raises:
            NotImplementedError: when itemType is not supported
        """
        if itemType != "item" and itemType != "tag" and itemType != "fluid" and itemType != "block" and itemType != "fluidTag":
            raise NotImplementedError("itemtype not supported" + itemType)

        slotInfo = {}
        slotInfo["type"] = itemType
        slotInfo["name"] = FixItemOrTagName(itemName)
        slotInfo["count"] = itemCount
        if nbt:
            slotInfo["nbtUnhashed"] = nbt
        self.__optionalOutputs.append(slotInfo)

    def SetOutputCount(self, count: int):
        """set amount of items produced by performing this recipe

        Args:
            count (number): items produced per craft
        """
        self.__outCount = count

    def SetType(self, type: str):
        """set recipe type

        Args:
            type (string): crafting handler that this recipe belongs too
        """
        self.__type = type

    def SetWidthAndHeight(self, width: int = 3, height: int = 3):
        """set width and height of grid for recipe (only needed on non standard dimensions)

        Args:
            width (int, optional): grid width. Defaults to 3.
            height (int, optional): grid height. Defaults to 3.
        """
        self.__width = width
        self.__height = height

    def PrepareForSerialization(self) -> dict[str, any]:
        """prepare recipe for serialization, do not call from outside

        Returns:
            dictionary: dictionary that can be serialized with json
        """
        output = {}
        output["count"] = self.__outCount
        if len(self.__optionalOutputs) > 0:
            output["optional"] = self.__optionalOutputs
        output["recipe"] = {}
        for i in self.__recipe:
            output["recipe"][i] = self.__recipe[i]
        output["type"] = self.__type
        if self.__height:
            output["height"] = self.__height
        if self.__width:
            output["width"] = self.__width
        return output


class ItemRecipe:
    """create recipe for item
    """

    def __init__(self):
        self.__outputName: str = ""
        self.__nbt: Union[str, None] = None
        self.__recipes: list[ItemRecipeRecipe] = []

    def SetOutputNameAndType(self, outputInfo: dict[str, any], nbtTag: str = "nbt"):
        """sets output name, and type for item produced by recipe
        Args:
            outputInfo (dict containing item | tag, count fields): information about output item
            nbtTag (string, optional): optional nbt tag name for output item. Defaults "nbt".
        """
        self.__outputType, self.__outputName, _, nbt = convertItemToInfo(
            outputInfo, nbtTag)
        self.__nbt = nbt

    def SetOutputNameAndTypeManually(self, type: str, name: str, nbt: Union[str, None] = None):
        """sets output name, type and nbt manually
        Args:
            type (string): output type
            name (string): output name
            nbt (string, optional): nbt tag info. Defaults to None.
        """
        self.__outputType = type
        self.__outputName = name
        self.__nbt = nbt

    def AddRecipe(self, recipe: ItemRecipeRecipe):
        """adds a recipe to craft this item
        Args:
            recipe (converterUtils.ItemRecipeRecipe): recipe to create this item
        """
        self.__recipes.append(recipe.PrepareForSerialization())

    def PrepareForSerialization(self) -> dict[str, any]:
        """prepare item recipe for serialization by python json class

        Returns:
            (dict) dictionary that can be serialized by python serializer
        """
        output = {}
        output["name"] = self.__outputName
        if self.__nbt != None:
            output["nbtUnhashed"] = self.__nbt
        output["type"] = self.__outputType

        output["recipes"] = []
        for i in self.__recipes:
            output["recipes"].append(i)

        return output

    def OutputIsSet(self) -> bool:
        """check if output has been set for recipe

        Returns:
            bool: true if set
        """
        return self.__outputName != ""


def FixItemOrTagName(name: str) -> str:
    """fixes items or tags that miss the "modId:" part in the recipe by putting in "minecraft:" if it is missing
    Args:
        name (string): name to verify for
    Returns:
        string: fixed tag or name
    """
    # some mod authors think being special is a good thing and therefor leave out the mod part of a tag or item id.
    # this obviously is stupid and needs fixing, this function fixes that
    if not ":" in name:
        # just put "minecraft:" infront and pray that it resolves to an actual item or tag
        print("item " + name +
              " is missing \"modId:\" adding \"minecraft:\" for modid as a bandaid")
        return "minecraft:" + name
    return name


def GetSlotsWithKey(pattern: list[str], key: str, cratingTableHorizontalSize: int = 3) -> list[int]:
    """find all slots with that key in a shaped recipe
    Args:
        pattern (string array): array of string which define a row in a crafting table recipe each
        key (string): key to find slots for
        craftingTableHorizontalSize (int): size of the crafting table, defaults to 3 
    Returns:
        array: array where each index is a slot the item occurs in
    """
    slots = []

    for row in range(len(pattern)):
        slotBaseNr = (row*cratingTableHorizontalSize) + 1
        for slot in range(len(pattern[row])):
            if pattern[row][slot] == key:
                slots.append(slotBaseNr + slot)
    return slots


def ConvertShapedPatternToSlotsInRecipe(recipe: dict[str, any], nbtTagName: Union[str, None] = "nbt", recipeTableHorizontalSize: int = 3) -> ItemRecipeRecipe:
    """convert a shaped minecraft like recipe to the ItemRecipeRecipe format, will not add output count for you
    Args:
        recipe (recipe dict): recipe to convert
        nbtTagName (str, optional): name for tag that saves nbt data in a key entry. Defaults to "nbt".
        recipeTableHorizontalSize (int, optional): horizontal size in slot for the used crafting table. Defaults to 3

    Returns:
        ItemRecipeRecipe: ItemRecipeRecipe without output count or optional outputs set
    """
    outRecipe = ItemRecipeRecipe()
    for key in recipe["key"]:
        slots = GetSlotsWithKey(
            recipe["pattern"], key, recipeTableHorizontalSize)
        for slot in slots:
            # todo fix this
            if type(recipe["key"][key]) is list:
                outRecipe.AddSlotInRecipe(
                    slot, recipe["key"][key][0], nbtTagName)
            else:
                outRecipe.AddSlotInRecipe(slot, recipe["key"][key], nbtTagName)
    return outRecipe


def ConvertUnshapedPatternToSlotsInRecipe(recipe: dict[str, any], nbtTagName: Union[str, None] = "nbt") -> ItemRecipeRecipe:
    """convert a unshaped minecraft like recipe to the ItemRecipeRecipe format, will not add output count for you
    Args:
        recipe (recipe dict): recipe to convert
        nbtTagName (str, optional): name for tag that saves nbt data in a key entry. Defaults to "nbt".

    Returns:
        ItemRecipeRecipe: ItemRecipeRecipe without output count or optional outputs set
    """
    outRecipe = ItemRecipeRecipe()

    slot = 1
    for item in recipe["ingredients"]:
        # todo generate more recipes when alternatives are detected
        if type(item) is list:
            outRecipe.AddSlotInRecipe(slot, item[0], nbtTagName)
        else:
            outRecipe.AddSlotInRecipe(slot, item, nbtTagName)
        slot += 1
    return outRecipe


def convertItemToInfo(item: dict[str, any], nbtTag: Union[str, None] = "nbt") -> tuple[str, str, int, Union[any, None]]:
    """convert item info from recipe to item information
    Args:
        item (item to extract info for): itemInfo as gotten from most recipes
        nbtTag (string, optional): nbt tag name in recipe. Defaults to "nbt" 
    Raises:
        NotImplementedError: if no converter for item type is implemented
    Returns:
        string, string, number, Union[any|None]: type, name, count, nbt
    """
    if "itemlist" in item:
        item = item["itemlist"]

    outputCount = 1
    if "count" in item:
        outputCount = item["count"]
    elif "amount" in item:
        outputCount = item["amount"]
    nbt = None
    if nbtTag in item:
        # todo: figure out how to encode it so cc doesnt throw a fit when comparing against itemDB
        # replace nbtUnhashed to nbt in this file when done
        nbt = item[nbtTag]

    if "item" in item:
        return "item", FixItemOrTagName(item["item"]), outputCount, nbt
    elif "name" in item:
        return "item", FixItemOrTagName(item["name"]), outputCount, nbt
    elif "tag" in item:
        return "tag", FixItemOrTagName(item["tag"]), outputCount, nbt
    elif "block" in item:
        return "block", FixItemOrTagName(item["block"]), outputCount, nbt
    elif "fluid" in item:
        return "fluid", FixItemOrTagName(item["fluid"]), outputCount, nbt
    elif "fluidTag" in item:
        return "fluidTag", FixItemOrTagName(item["fluidTag"]), outputCount, nbt
    else:
        raise NotImplementedError("no conversion implemented for " + str(item))
