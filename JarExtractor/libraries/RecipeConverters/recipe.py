import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils
#minecolonies has a recipe type called recipe because that cant cause problems with other mods ever

def ConvertRecipeRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "recipe":
        return None
    utils.Print("no recipe converter for " + Type)
    return None