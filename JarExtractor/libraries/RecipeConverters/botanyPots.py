import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def ConvertBotanyPotsRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    #makes no sense to convert botany pot recipes
    if Type == "botanypots:crop":
        return None
    elif Type == "botanypots:fertilizer":
        return None
    elif Type == "botanypots:soil":
        return None
    utils.Print("no recipe converter for " + Type)
    return None




#used for testing conversions
#testRecipeFileName = "botania_gog_alternation"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertBotanyPotsRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()