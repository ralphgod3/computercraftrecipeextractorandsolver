import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __Convertplanttech2CompressingRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    recipe = converterUtils.ItemRecipeRecipe()
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    itemType, itemName, itemCount, itemNbt = converterUtils.convertItemToInfo(
        recipeToConvert["input"])
    recipe.AddSlotInRecipeManually(1, itemType, itemName, itemCount, itemNbt)
    recipe.SetOutputCount(1)
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def ConvertTemplateRecipe(recipeToConvert):
    typesToIgnore = [
        "planttech2:chipalyzer",
        "planttech2:infusing",
    ]

    Type = recipeToConvert["type"]
    # check against non automateable types
    for t in typesToIgnore:
        if t == Type:
            return None
    if Type == "planttech2:compressing":
        return __Convertplanttech2CompressingRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None


# used for testing conversions
# testRecipeFileName = "minecraft_smoking"
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    Type, converted = ConvertMinecraftRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
# testOutput = json.dumps(convertedRecipes, indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
