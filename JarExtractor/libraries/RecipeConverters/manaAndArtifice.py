import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

#when i actually start playing with this mod i might know what is automatable for crafting
def ConvertManaAndArtificeRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "mana-and-artifice:arcane-furnace":
        return None
    elif Type == "mana-and-artifice:component":
        return None
    elif Type == "mana-and-artifice:manaweaving-pattern":
        return None
    elif Type == "mana-and-artifice:manaweaving-recipe":
        return None
    elif Type == "mana-and-artifice:modifier":
        return None
    elif Type == "mana-and-artifice:ritual":
        return None
    elif Type == "mana-and-artifice:runeforging":
        return None
    elif Type == "mana-and-artifice:runescribing":
        return None
    elif Type == "mana-and-artifice:shape":
        return None
    
    utils.Print("no recipe converter for " + Type)
    return None