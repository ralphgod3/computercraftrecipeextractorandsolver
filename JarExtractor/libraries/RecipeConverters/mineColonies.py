import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils
#I dont have enough experience with minecolonies to even know if these can even be made by a human
#so this file is a placeholder to suppress warnings until i can figure out if they do

def ConvertMineColoniesRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "minecolonies":
        return None
    elif Type == "minecolonies:composting":
        return None
    utils.Print("no recipe converter for " + Type)
    return None



##used for testing conversions
#testRecipeFileName = "mekanism_sawing"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertMineColoniesRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()