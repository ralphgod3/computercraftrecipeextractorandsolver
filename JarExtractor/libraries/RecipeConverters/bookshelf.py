import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def ConvertBookshelfRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    #useless mod only 3 recipes which all will never need autocrafts
    if Type == "bookshelf:crafting_shapeless_with_damage":
        return None
    elif Type == "bookshelf:smithing_font":
        return None
    elif Type == "bookshelf:smithing_repair_cost":
        return None
    utils.Print("no recipe converter for " + Type)
    return None




#used for testing convertes
#testRecipeFileName = "bookshelf_crafting_shapeless_with_damage"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertBookshelfRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()