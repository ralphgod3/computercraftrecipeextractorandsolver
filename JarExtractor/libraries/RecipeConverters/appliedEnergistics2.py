import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertAE2GrinderRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()

    if "optional" in recipeToConvert["result"]:
        for opt in recipeToConvert["result"]["optional"]:
            recipe.AddOptionalOutput(opt)

    if "count" in recipeToConvert["result"]["primary"]:
        recipe.SetOutputCount(recipeToConvert["result"]["primary"]["count"])

    recipe.AddSlotInRecipe(1, recipeToConvert["input"])

    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"]["primary"])
    return outRecipe.PrepareForSerialization()


def __ConvertAE2InscriberRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()

    if "count" in recipeToConvert["result"]:
        recipe.SetOutputCount(recipeToConvert["result"]["count"])

    slot = 1
    for ingredient in recipeToConvert["ingredients"]:
        recipe.AddSlotInRecipe(
            slot, recipeToConvert["ingredients"][ingredient])
        slot += 1

    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    del recipe
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def __ConvertAE2ChargerRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()

    if "count" in recipeToConvert["result"]:
        recipe.SetOutputCount(recipeToConvert["result"]["count"])
    recipe.AddSlotInRecipe(1, recipeToConvert["ingredient"])
    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    del recipe
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def __ConvertAE2TransformRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()

    if "count" in recipeToConvert["result"]:
        recipe.SetOutputCount(recipeToConvert["result"]["count"])

    slot = 1
    for ingredient in recipeToConvert["ingredients"]:
        recipe.AddSlotInRecipe(
            slot, ingredient)
        slot += 1

    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    del recipe
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def ConvertAE2Recipe(recipeToConvert):
    Type = recipeToConvert["type"]

    typesToIgnore = [
        "appliedenergistics2:disassemble",
        "ae2:disassemble",
        "appliedenergistics2:entropy",
        "ae2:entropy",
        "appliedenergistics2:facade",
        "ae2:facade",
        "ae2:matter_cannon"
    ]

    for t in typesToIgnore:
        if t == Type:
            return None

    if Type == "appliedenergistics2:grinder" or Type == "ae2:grinder":
        return __ConvertAE2GrinderRecipe(recipeToConvert, Type)
    elif Type == "appliedenergistics2:inscriber" or Type == "ae2:inscriber":
        return __ConvertAE2InscriberRecipe(recipeToConvert, Type)
    elif Type == "ae2:charger":
        return __ConvertAE2ChargerRecipe(recipeToConvert, Type)
    elif Type == "ae2:transform":
        return __ConvertAE2TransformRecipe(recipeToConvert, Type)

    utils.Print("no recipe converter for " + Type)
    return None


# used for testing convertes
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\appliedenergistics2_inscriber.json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    _, converted = ConvertAE2Recipe(recipe["recipe"])
#    convertedRecipes.append(converted)
#
#
#
#
#
# testOutput = json.dumps(convertedRecipes,indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
