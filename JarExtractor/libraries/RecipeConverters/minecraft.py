import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertMinecraftBlastingRecipe(recipeToConvert, recipeType):
    if type(recipeToConvert["result"]) is dict:
        Type, name, count, nbt = converterUtils.convertItemToInfo(
            recipeToConvert["result"])
    else:
        Type = "item"
        name = recipeToConvert["result"]
        count = 1
        nbt = None
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    if type(recipeToConvert["ingredient"]) is list:
        for ing in recipeToConvert["ingredient"]:
            recipe = converterUtils.ItemRecipeRecipe()
            recipe.SetOutputCount(count)
            recipe.SetType(recipeType)
            recipe.AddSlotInRecipe(1, ing)
            outRecipe.AddRecipe(recipe)
    else:
        recipe = converterUtils.ItemRecipeRecipe()
        recipe.SetOutputCount(count)
        recipe.AddSlotInRecipe(1, recipeToConvert["ingredient"])
        recipe.SetType(recipeType)
        outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def __ConvertMinecraftCampfireCookingRecipe(recipeToConvert, recipeType):
    return __ConvertMinecraftBlastingRecipe(recipeToConvert, recipeType)


def __ConvertMinecraftShapedCraftingRecipe(recipeToConvert, recipeType):
    typesToIgnore = [
        "bloodmagic:bloodorb",  # blood magic
        "mantle:without"  # tinkers construct
    ]
    for key in recipeToConvert["key"]:
        for t in typesToIgnore:
            if "type" in recipeToConvert["key"][key] and t in recipeToConvert["key"][key]["type"]:
                return None
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = converterUtils.convertItemToInfo(
        recipeToConvert["result"])
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def __ConvertMinecraftShapelessCraftingRecipe(recipeToConvert, recipeType):
    typesToIgnore = [
        "bloodmagic:bloodorb",  # blood magic
        "mantle:without",  # tinkers construct
        "tetra:scroll",  # tetra
        "forge:difference",  # not used by alot and would be aan absolute pain to implement
        "l2library:enchantment"
    ]
    for ingredient in recipeToConvert["ingredients"]:
        for t in typesToIgnore:
            if "type" in ingredient and t in ingredient["type"]:
                return None
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = converterUtils.convertItemToInfo(
        recipeToConvert["result"])
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def __ConvertMinecraftSmeltingRecipe(recipeToConvert, recipeType):
    return __ConvertMinecraftBlastingRecipe(recipeToConvert, recipeType)


def __ConvertMinecraftSmokingRecipe(recipeToConvert, recipeType):
    return __ConvertMinecraftCampfireCookingRecipe(recipeToConvert, recipeType)


def ConvertMinecraftRecipe(recipeToConvert):
    notAutomateableRecipes = [
        "minecraft:stonecutting",
        "minecraft:crafting_special_armordye",
        "minecraft:crafting_special_bannerduplicate",
        "minecraft:crafting_special_bookcloning",
        "minecraft:smithing_trim",
        "minecraft:crafting_decorated_pot",
        "minecraft:crafting_special_firework_rocket",
        "minecraft:crafting_special_firework_star",
        "minecraft:crafting_special_firework_star_fade",
        "minecraft:crafting_special_mapcloning",
        "minecraft:crafting_special_mapextending",
        "minecraft:crafting_special_repairitem",
        "minecraft:smithing_transform",
        "minecraft:crafting_special_shielddecoration",
        "minecraft:crafting_special_shulkerboxcoloring",
        "minecraft:crafting_special_suspiciousstew",
        "minecraft:crafting_special_tippedarrow",
    ]

    Type = recipeToConvert["type"]
    # check against non automateable types
    for t in notAutomateableRecipes:
        if t == Type:
            return None
    if Type == "minecraft:blasting":
        return __ConvertMinecraftBlastingRecipe(recipeToConvert, Type)
    elif Type == "minecraft:campfire_cooking":
        return __ConvertMinecraftCampfireCookingRecipe(recipeToConvert, Type)
    elif Type == "minecraft:crafting_shaped":
        return __ConvertMinecraftShapedCraftingRecipe(recipeToConvert, Type)
    elif Type == "minecraft:crafting_shapeless":
        return __ConvertMinecraftShapelessCraftingRecipe(recipeToConvert, Type)
    elif Type == "minecraft:smelting":
        return __ConvertMinecraftSmeltingRecipe(recipeToConvert, Type)
    elif Type == "minecraft:smithing":  # dont see any use for these at the moment
        return None
    elif Type == "minecraft:smoking":
        return __ConvertMinecraftSmokingRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None


# used for testing conversions
# testRecipeFileName = "minecraft_smoking"
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    Type, converted = ConvertMinecraftRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
# testOutput = json.dumps(convertedRecipes, indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
