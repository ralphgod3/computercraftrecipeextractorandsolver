import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def __ConvertEnderstorageCreateRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(recipeToConvert)
    _,_,count,_ = converterUtils.convertItemToInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    recipe.SetType(converterUtils.craftingHandler)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()

def ConvertEnderstorageRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "enderstorage:create_recipe":
        return __ConvertEnderstorageCreateRecipe(recipeToConvert, Type)
    elif Type == "enderstorage:recolour_recipe":
        return None
    
    utils.Print("no recipe converter for " + Type)
    return None




##used for testing conversions
#testRecipeFileName = "enderstorage_create_recipe"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertEnderstorageRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()