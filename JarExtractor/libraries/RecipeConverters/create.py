# not a mod but handles shaped and shapeless recipes of type crafting since appearently minecraft allows people to leave out the minecraft: part of the handler
# and worse some people actually do that

import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertCreateCompactingRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(recipeType)
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["results"][0])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["results"][0])
    return outRecipe.PrepareForSerialization()


def __ConvertCreateCrushingRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(recipeType)
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["results"][0])
    recipe.SetOutputCount(count)
    for i in range(len(recipeToConvert["results"])):
        if i != 0:
            recipe.AddOptionalOutput(recipeToConvert["results"][i])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["results"][0])
    return outRecipe.PrepareForSerialization()


def __ConvertCreateCuttingRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(recipeType)
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["results"][0])
    recipe.SetOutputCount(count)
    for i in range(len(recipeToConvert["results"])):
        if i != 0:
            recipe.AddOptionalOutput(recipeToConvert["results"][i])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["results"][0])
    return outRecipe.PrepareForSerialization()


def __ConvertCreateFillingRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(recipeType)
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["results"][0])
    recipe.SetOutputCount(count)
    for i in range(len(recipeToConvert["results"])):
        if i != 0:
            recipe.AddOptionalOutput(recipeToConvert["results"][i])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["results"][0])
    return outRecipe.PrepareForSerialization()


def __ConvertCreateMechanicalCraftingRecipe(recipeToConvert, recipeType):
    maxCrafterDimension = len(recipeToConvert["pattern"])
    for strings in recipeToConvert["pattern"]:
        if len(strings) > maxCrafterDimension:
            maxCrafterDimension = len(strings)
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(
        recipeToConvert, "nbt", maxCrafterDimension)
    recipe.SetType(recipeType)
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def __ConvertCreateMillingRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(recipeType)
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["results"][0])
    recipe.SetOutputCount(count)
    for i in range(len(recipeToConvert["results"])):
        if i != 0:
            recipe.AddOptionalOutput(recipeToConvert["results"][i])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["results"][0])
    return outRecipe.PrepareForSerialization()


def __ConvertCreateMixingRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(recipeType)
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["results"][0])
    recipe.SetOutputCount(count)
    for i in range(len(recipeToConvert["results"])):
        if i != 0:
            recipe.AddOptionalOutput(recipeToConvert["results"][i])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["results"][0])
    return outRecipe.PrepareForSerialization()


def __ConvertCreatePressingRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["results"][0])
    recipe.SetType(recipeType)
    recipe.SetOutputCount(count)
    for i in range(len(recipeToConvert["results"])):
        if i != 0:
            recipe.AddOptionalOutput(recipeToConvert["results"][i])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["results"][0])
    return outRecipe.PrepareForSerialization()


def __ConvertCreateSandpaperPolishingRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["results"][0])
    recipe.SetOutputCount(count)
    recipe.SetType(recipeType)
    for i in range(len(recipeToConvert["results"])):
        if i != 0:
            recipe.AddOptionalOutput(recipeToConvert["results"][i])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["results"][0])
    return outRecipe.PrepareForSerialization()


def __ConvertCreateSplashingRecipe(recipeToConvert, recipeType):
    # chance recipes suck to deal with in autocrafting
    if "chance" in recipeToConvert["results"][0]:
        return None
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(recipeType)
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["results"][0])
    recipe.SetOutputCount(count)
    for i in range(len(recipeToConvert["results"])):
        if i != 0:
            recipe.AddOptionalOutput(recipeToConvert["results"][i])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["results"][0])
    return outRecipe.PrepareForSerialization()


def __ConvertCreateDeployingRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["results"][0])
    recipe.SetType(recipeType)
    recipe.SetOutputCount(count)
    for i in range(len(recipeToConvert["results"])):
        if i != 0:
            recipe.AddOptionalOutput(recipeToConvert["results"][i])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["results"][0])
    return outRecipe.PrepareForSerialization()


def __ConvertCreateHauntingRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["results"][0])
    if "chance" in recipeToConvert["results"][0]:
        return None
    recipe.SetType(recipeType)
    recipe.SetOutputCount(count)
    for i in range(len(recipeToConvert["results"])):
        if i != 0:
            recipe.AddOptionalOutput(recipeToConvert["results"][i])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["results"][0])
    return outRecipe.PrepareForSerialization()


def ConvertCreateRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    typesToIgnore = [
        "create:blockzapper_upgrade",
        "create:emptying",
        "create:toolbox_dyeing",
        "create:sequenced_assembly",
        "create:item_application",
    ]
    for t in typesToIgnore:
        if t == Type:
            return None

    if Type == "create:compacting":
        return __ConvertCreateCompactingRecipe(recipeToConvert, Type)
    elif Type == "create:crushing":
        return __ConvertCreateCrushingRecipe(recipeToConvert, Type)
    elif Type == "create:cutting":
        return __ConvertCreateCuttingRecipe(recipeToConvert, Type)
    elif Type == "create:filling":
        return __ConvertCreateFillingRecipe(recipeToConvert, Type)
    elif Type == "create:mechanical_crafting":
        return __ConvertCreateMechanicalCraftingRecipe(recipeToConvert, Type)
    elif Type == "create:milling":
        return __ConvertCreateMillingRecipe(recipeToConvert, Type)
    elif Type == "create:mixing":
        return __ConvertCreateMixingRecipe(recipeToConvert, Type)
    elif Type == "create:pressing":
        return __ConvertCreatePressingRecipe(recipeToConvert, Type)
    elif Type == "create:sandpaper_polishing":
        return __ConvertCreateSandpaperPolishingRecipe(recipeToConvert, Type)
    elif Type == "create:splashing":
        return __ConvertCreateSplashingRecipe(recipeToConvert, Type)
    elif Type == "create:deploying":
        return __ConvertCreateDeployingRecipe(recipeToConvert, Type)
    elif Type == "create:haunting":
        return __ConvertCreateHauntingRecipe(recipeToConvert, Type)
    elif Type == "create:application":
        return __ConvertCreateHauntingRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None


# used for testing conversions
# testRecipeFileName = "create_splashing"
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    Type, converted = ConvertCraftingRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
# testOutput = json.dumps(convertedRecipes, indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
