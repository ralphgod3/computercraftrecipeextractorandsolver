import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def __ConvertIndustrialForegoingCrusherRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    recipe.AddSlotInRecipe(1, recipeToConvert["input"])
    _,_,count,_ = converterUtils.convertItemToInfo(recipeToConvert["output"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    return outRecipe.PrepareForSerialization()

def ConvertIndustrialForegoingRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "industrialforegoing:crusher":
        return __ConvertIndustrialForegoingCrusherRecipe(recipeToConvert, Type)
    elif Type == "industrialforegoing:dissolution_chamber": #maybe later
        return None
    elif Type == "industrialforegoing:fluid_extractor": #who the hell would set recipes by computer for this
        return None
    elif Type == "industrialforegoing:laser_drill_fluid": # what are these recipes even?
        return None
    elif Type == "industrialforegoing:laser_drill_ore": #not a recipe
        return None
    elif Type == "industrialforegoing:stonework_generate": #recipe setting not automatable
        return None
    
    utils.Print("no recipe converter for " + Type)
    return None




##used for testing conversions
#testRecipeFileName = "industrialforegoing_crusher"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertIndustrialForegoingRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()