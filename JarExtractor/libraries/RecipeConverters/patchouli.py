import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertPatchouliShapelessCraftingRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    Type = "item"
    name = recipeToConvert["book"]
    count = 1
    outRecipe.SetOutputNameAndTypeManually(Type, name)
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def ConvertPatchouliRecipe(recipeToConvert):
    typesToIgnore = [
    ]

    Type = recipeToConvert["type"]
    # check against non automateable types
    for t in typesToIgnore:
        if t == Type:
            return None
    if Type == "patchouli:shapeless_book_recipe":
        return __ConvertPatchouliShapelessCraftingRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None


# used for testing conversions
# testRecipeFileName = "minecraft_smoking"
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    Type, converted = ConvertMinecraftRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
# testOutput = json.dumps(convertedRecipes, indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
