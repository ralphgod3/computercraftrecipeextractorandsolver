import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertComputercraftComputerupgradeRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(
        recipeToConvert)
    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(converterUtils.craftingHandler)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def __ConvertComputercraftImposterShapedRecipe(recipeToConvert, recipeType):
    # check if output item exactly matches an input item CC crafting handlers are derpy :s
    if "nbt" not in recipeToConvert["result"]:
        for key in recipeToConvert["key"]:
            if "item" in recipeToConvert["key"][key] and recipeToConvert["result"]["item"] == recipeToConvert["key"][key]["item"]:
                return None
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def __ConvertComputercraftImposterShapelessRecipe(recipeToConvert, recipeType):
    # check if output item exactly matches an input item CC crafting handlers are derpy :s
    if "nbt" not in recipeToConvert["result"]:
        for ingredient in recipeToConvert["ingredients"]:
            if "item" in ingredient and recipeToConvert["result"]["item"] == ingredient["item"]:
                return None
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def __ConvertComputercraftTurtleRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def ConvertComputercraftRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    typesToIgnore = [
        "turtle_overlay",
        "computercraft:colour",
        "computercraft:clear_colour",
        "computercraft:disk",
        "computercraft:pocket_computer_upgrade",
        "computercraft:printout",
        "computercraft:turtle_upgrade",
        "computercraft:turtle_overlay"
    ]
    for t in typesToIgnore:
        if t == Type:
            return None

    if Type == "computercraft:computer_upgrade":
        return __ConvertComputercraftComputerupgradeRecipe(recipeToConvert, Type)
    elif Type == "computercraft:impostor_shaped":
        return __ConvertComputercraftImposterShapedRecipe(recipeToConvert, Type)
    elif Type == "computercraft:impostor_shapeless":
        return __ConvertComputercraftImposterShapelessRecipe(recipeToConvert, Type)
    elif Type == "computercraft:turtle":
        return __ConvertComputercraftTurtleRecipe(recipeToConvert, Type)

    utils.Print("no recipe converter for " + Type)
    return None


# used for testing conversions
# testRecipeFileName = "computercraft_turtle"
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    Type, converted = ConvertComputercraftRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
# testOutput = json.dumps(convertedRecipes, indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
