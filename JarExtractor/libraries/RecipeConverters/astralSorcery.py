import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def __ConvertAstralSorceryInfuserRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.AddSlotInRecipeManually(1, "fluid", recipeToConvert["fluidInput"],  recipeToConvert["consumptionChance"]*1000)
    recipe.AddSlotInRecipe(2, recipeToConvert["input"])
    _,_, count,_ = converterUtils.convertItemToInfo(recipeToConvert["output"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    return outRecipe.PrepareForSerialization()

def ConvertAstralSorceryRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "astralsorcery:altar":
        return None
    elif Type == "astralsorcery:block_transmutation":
        return None
    elif Type == "astralsorcery:change_gateway_color":
        return None
    elif Type == "astralsorcery:change_wand_color":
        return None
    elif Type == "astralsorcery:infuser":
        return __ConvertAstralSorceryInfuserRecipe(recipeToConvert, Type)
    elif Type == "astralsorcery:lightwell":
        return None #chance based recipes SUCK
    elif Type == "astralsorcery:liquid_interaction":
        return None
    utils.Print("no recipe converter for " + Type)
    return None




#used for testing convertes
#testRecipeFileName = "astralsorcery_lightwell"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    _, converted = ConvertAstralSorceryRecipe(recipe["recipe"])
#    convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()