import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertEndertanksCraftingRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = converterUtils.convertItemToInfo(
        recipeToConvert["result"])
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def ConvertEndertanksRecipe(recipeToConvert):
    typesToIgnore = [
    ]

    Type = recipeToConvert["type"]
    # check against non automateable types
    for t in typesToIgnore:
        if t == Type:
            return None
    if Type == "endertanks:crafting":
        return __ConvertEndertanksCraftingRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None


# used for testing conversions
# testRecipeFileName = "minecraft_smoking"
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    Type, converted = ConvertMinecraftRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
# testOutput = json.dumps(convertedRecipes, indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
