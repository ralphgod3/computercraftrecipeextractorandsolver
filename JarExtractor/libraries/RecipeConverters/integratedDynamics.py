import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertIntegratedDynamicsDryingBasinRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    slot = 1
    if "item" in recipeToConvert:
        if type(recipeToConvert["item"]) is dict:
            recipe.AddSlotInRecipeManually(
                slot, "tag", recipeToConvert["item"]["tag"])
        else:
            recipe.AddSlotInRecipeManually(
                slot, "item", recipeToConvert["item"])
        slot += 1
    if "fluid" in recipeToConvert:
        recipe.AddSlotInRecipe(slot, recipeToConvert["fluid"])
    _, _, count, _ = converterUtils.convertItemToInfo(
        recipeToConvert["result"])
    recipe.SetOutputCount(count)
    recipe.SetType(recipeType)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def ConvertIntegratedDynamicsRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    typesToIgnore = [
        "integrateddynamics:crafting_special_energycontainer_combination",
        "integrateddynamics:crafting_special_facade",
        "integrateddynamics:crafting_special_nbt_clear",
        "integrateddynamics:crafting_special_variable_copy",
        # this recipe structure is just too stupid to waste time on
        "integrateddynamics:mechanical_squeezer",
        # this recipe structure is just too stupid to waste time on
        "integrateddynamics:squeezer",
        "integrateddynamics:crafting_special_shaped_omni_directional",
        "integrateddynamics:crafting_special_shaped_omni_directional_3",
        "integrateddynamics:crafting_special_shapeless_omni_directional",
    ]
    for t in typesToIgnore:
        if t == Type:
            return None

    if Type == "integrateddynamics:drying_basin":
        return __ConvertIntegratedDynamicsDryingBasinRecipe(recipeToConvert, Type)
    elif Type == "integrateddynamics:mechanical_drying_basin":
        return __ConvertIntegratedDynamicsDryingBasinRecipe(recipeToConvert, Type)

    utils.Print("no recipe converter for " + Type)
    return None


# used for testing conversions
# testRecipeFileName = "integrateddynamics_mechanical_squeezer"
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    Type, converted = ConvertIntegratedDynamicsRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
# testOutput = json.dumps(convertedRecipes, indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
