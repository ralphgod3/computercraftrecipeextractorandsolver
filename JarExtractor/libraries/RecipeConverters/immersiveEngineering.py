import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


#Immersive engineering wants to be special and has a base ingredient tag which is annoying as FUCK whilst serving no purpose whatsoever
#this function removes it and converts it to a normal itemInfo return
def __ConvertIEResourseToItemInfo(inputItemInfo):
    if "output" in inputItemInfo:
        inputItemInfo = inputItemInfo["output"]
    if type(inputItemInfo) is list:
        inputItemInfo = inputItemInfo[0]
    count = 1
    if "base_ingredient" in inputItemInfo:
        if type(inputItemInfo) is list:
            inputItemInfo = inputItemInfo[0]
        if "count" in inputItemInfo["base_ingredient"]:
            count = inputItemInfo["base_ingredient"]["count"]
        Type, name, _, nbt = converterUtils.convertItemToInfo(inputItemInfo["base_ingredient"])
        if "type" in inputItemInfo["base_ingredient"]:
            if inputItemInfo["base_ingredient"]["type"] == "immersiveengineering:fluid":
                inputItemInfo["base_ingredient"]["type"] = "fluid"
            else:
                raise NotImplementedError("type not implemented " + inputItemInfo["type"])
        return Type, name, count, nbt
    else:
        Type, name, count, nbt = converterUtils.convertItemToInfo(inputItemInfo)
        if "type" in inputItemInfo:
            if inputItemInfo["type"] == "immersiveengineering:fluid":
                Type = "fluid"
            else:
                raise NotImplementedError("type not implemented " + inputItemInfo["type"])
        return Type, name, count, nbt

def __ConvertImmersiveEngineeringBlastFurnaceRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["input"])
    recipe.AddSlotInRecipeManually(1, Type, name, count, nbt)
    recipe.AddSlotInRecipe(1, recipeToConvert["input"])
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["slag"])
    recipe.AddOptionalOutputManually(Type, name, count, nbt)
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    return outRecipe.PrepareForSerialization()

def __ConvertImmersiveEngineeringBlueprintRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    slot = 1
    for input in recipeToConvert["inputs"]:
        Type, name, count, nbt = __ConvertIEResourseToItemInfo(input)
        recipe.AddSlotInRecipeManually(slot, Type, name, count, nbt)
        slot+=1
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    return outRecipe.PrepareForSerialization()

def __ConvertImmersiveEngineeringBottlingMachineRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    recipe.AddSlotInRecipe(1, recipeToConvert["input"])
    recipe.AddSlotInRecipeManually(2, "fluid",recipeToConvert["fluid"]["tag"], recipeToConvert["fluid"]["amount"])
    _,_,count,_ = converterUtils.convertItemToInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()

def __ConvertImmersiveEngineeringCrusherRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    recipe.AddSlotInRecipe(1, recipeToConvert["input"])
    for secondary in recipeToConvert["secondaries"]:
        recipe.AddOptionalOutput(secondary["output"])
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe.SetOutputNameAndTypeManually(Type,name,nbt)
    return outRecipe.PrepareForSerialization()

def __ConvertImmersiveEngineeringHammerCrushingRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    recipe.AddSlotInRecipe(1, recipeToConvert["input"])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()

def __ConvertImmersiveEngineeringMetalPressRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["input"])
    recipe.AddSlotInRecipeManually(1, Type, name, count, nbt)
    if isinstance(recipeToConvert["mold"], str):
        return None
    recipe.AddSlotInRecipe(2, recipeToConvert["mold"])
    recipe.AddOptionalOutput(recipeToConvert["mold"])
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndTypeManually(Type,name, nbt)
    return outRecipe.PrepareForSerialization()

def __ConvertImmersiveEngineeringMixerRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    slot = 1
    for input in recipeToConvert["inputs"]:
        Type, name, count, nbt = __ConvertIEResourseToItemInfo(input)
        recipe.AddSlotInRecipeManually(slot, Type, name, count, nbt)
        slot+=1
    recipe.AddSlotInRecipeManually(slot, "fluid",recipeToConvert["fluid"]["tag"], recipeToConvert["fluid"]["amount"])
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    return outRecipe.PrepareForSerialization()

def __ConvertImmersiveEngineeringSawmillRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["input"])
    recipe.AddSlotInRecipeManually(1, Type, name, count, nbt)
    for secondary in recipeToConvert["secondaries"]:
        Type, name, count, nbt = __ConvertIEResourseToItemInfo(secondary)
        recipe.AddOptionalOutputManually(Type, name, count, nbt)
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndTypeManually(Type,name, nbt)
    return outRecipe.PrepareForSerialization()

def __ConvertImmersiveEngineeringShapedFluidRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(recipeToConvert)
    recipe.SetType(recipeType)    
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    return outRecipe.PrepareForSerialization()

def __ConvertImmersiveEngineeringShapelessFluidRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(recipeToConvert)
    recipe.SetType(recipeType)
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    return outRecipe.PrepareForSerialization()

def __ConvertImmersiveEngineeringSqueezerRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["input"])
    recipe.AddSlotInRecipeManually(1, Type, name, count, nbt)
    #really they needed a extra tag for output for a non fluid output for 1 RECIPE
    key = "result"
    if "fluid" in recipeToConvert:
        key = "fluid"
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert[key])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    return outRecipe.PrepareForSerialization()
    
def __ConvertImmersiveEngineeringTurnAndCopyRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    for key in recipeToConvert["key"]:
        slots = converterUtils.GetSlotsWithKey(recipeToConvert["pattern"] ,key)
        for slot in slots:
            #todo fix this
            if type(recipeToConvert["key"][key]) is list:
                recipeToConvert["key"][key] = recipeToConvert["key"][key][0]
            Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["key"][key])
            if "type" in recipeToConvert["key"][key]:
                if recipeToConvert["key"][key]["type"] == "immersiveengineering:fluid":
                    Type = "fluid"
                else:
                    raise NotImplementedError("type not implemented " + recipeToConvert["key"][key]["type"])
            recipe.AddSlotInRecipeManually(slot, Type, name, count, nbt)
    Type, name, count, nbt = __ConvertIEResourseToItemInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    recipe.SetType(recipeType)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    return outRecipe.PrepareForSerialization()


def ConvertImmersiveEngineeringRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "immersiveengineering:alloy": #maybe add later when conditional checking is implemented
        return None
    elif Type == "immersiveengineering:arc_furnace": #maybe add later when conditional checking is implemented
        return None
    elif Type == "immersiveengineering:blast_furnace": #add
        return __ConvertImmersiveEngineeringBlastFurnaceRecipe(recipeToConvert, Type)
    elif Type == "immersiveengineering:blast_furnace_fuel":
        return None
    elif Type == "immersiveengineering:blueprint":
        return __ConvertImmersiveEngineeringBlueprintRecipe(recipeToConvert, Type)
    elif Type == "immersiveengineering:bottling_machine":
        return __ConvertImmersiveEngineeringBottlingMachineRecipe(recipeToConvert, Type)
    elif Type == "immersiveengineering:cloche": #cant remove items from cloche after initial setup, no use converting
        return None
    elif Type == "immersiveengineering:coke_oven": #recipe format not setup to deal with creosote fluid outputs atm
        return None
    elif Type == "immersiveengineering:crafting_special_flare_bullet_color":
        return None
    elif Type == "immersiveengineering:crafting_special_jerrycan_refill":
        return None
    elif Type == "immersiveengineering:crafting_special_potion_bullet_fill":
        return None
    elif Type == "immersiveengineering:crafting_special_speedloader_load":
        return None
    elif Type == "immersiveengineering:crusher":
        return __ConvertImmersiveEngineeringCrusherRecipe(recipeToConvert, Type)
    elif Type == "immersiveengineering:earmuffs":
        return None
    elif Type == "immersiveengineering:fermenter": #not gonna autocraft this for now
        return None
    elif Type == "immersiveengineering:fertilizer":
        return None
    elif Type == "immersiveengineering:generated_list":
        return None
    elif Type == "immersiveengineering:hammer_crushing":
        return __ConvertImmersiveEngineeringHammerCrushingRecipe(recipeToConvert, Type)
    elif Type == "immersiveengineering:ie_item_repair":
        return None
    elif Type == "immersiveengineering:metal_press":
        return __ConvertImmersiveEngineeringMetalPressRecipe(recipeToConvert, Type)
    elif Type == "immersiveengineering:mineral_mix": #part of oregen for excavator
        return None
    elif Type == "immersiveengineering:mixer":
        return __ConvertImmersiveEngineeringMixerRecipe(recipeToConvert, Type)
    elif Type == "immersiveengineering:powerpack":
        return None
    elif Type == "immersiveengineering:refinery": #never a manual operation
        return None
    elif Type == "immersiveengineering:revolver_assembly":
        return None
    elif Type == "immersiveengineering:revolver_cycle":
        return None
    elif Type == "immersiveengineering:rgb":
        return None
    elif Type == "immersiveengineering:sawmill":
        return __ConvertImmersiveEngineeringSawmillRecipe(recipeToConvert, Type)
    elif Type == "immersiveengineering:shaped_fluid":
        return __ConvertImmersiveEngineeringShapedFluidRecipe(recipeToConvert, Type)
    elif Type == "immersiveengineering:shapeless_fluid":
        return __ConvertImmersiveEngineeringShapelessFluidRecipe(recipeToConvert, Type)
    elif Type == "immersiveengineering:squeezer":
        return __ConvertImmersiveEngineeringSqueezerRecipe(recipeToConvert, Type)
    elif Type == "immersiveengineering:turn_and_copy":
        return __ConvertImmersiveEngineeringTurnAndCopyRecipe(recipeToConvert, Type)
    
    utils.Print("no recipe converter for " + Type)
    return None, None

##used for testing conversions
#testRecipeFileName = "immersiveengineering_turn_and_copy"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertImmersiveEngineeringRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()