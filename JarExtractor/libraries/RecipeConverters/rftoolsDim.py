import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def ConvertRftoolsDimRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "rftoolsdim:dimlet_cycle_recipe": #cant convert
        return None
    elif Type == "rftoolsdim:dimlet_recipe": #cant convert
        return None
    elif Type == "rftoolsdim:spawner": #no recipe
        return None
    utils.Print("no recipe converter for " + Type)
    return None

##used for testing conversions
#testRecipeFileName = "rats"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertPsiRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()