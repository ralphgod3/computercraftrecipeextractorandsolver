import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertPneumaticCraftCraftingShapedNoMirrorRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = converterUtils.convertItemToInfo(
        recipeToConvert["result"])
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def __ConvertPneumaticCraftPressureChamberRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = converterUtils.convertItemToInfo(
        recipeToConvert["results"][0])
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    first = True
    for result in recipeToConvert["results"]:
        if first:
            first = False
        else:
            recipe.AddOptionalOutput(result)
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    slot = 1
    for input in recipeToConvert["inputs"]:
        recipe.AddSlotInRecipe(slot, input)
        slot += 1
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def ConvertPneumaticCraftRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    typesToIgnore = [
        "pneumaticcraft:amadron",  # amadron offers are not crafting recipes
        "pneumaticcraft:assembly_drill",  # no clue what this is
        "pneumaticcraft:assembly_laser",  # no clue what this is
        "pneumaticcraft:drone_color_crafting",  # no recipe
        "pneumaticcraft:drone_upgrade_crafting",  # no recipe
        "pneumaticcraft:explosion_crafting",  # chance based recipe
        "pneumaticcraft:fluid_mixer",  # fluid based, maybe later
        "pneumaticcraft:fuel_quality",  # no recipe
        "pneumaticcraft:gun_ammo_potion_crafting",  # no recipe
        "pneumaticcraft:heat_frame_cooling",  # no recipe
        "pneumaticcraft:heat_properties",  # no recipe
        "pneumaticcraft:pressure_chamber_disenchanting",  # no recipe
        "pneumaticcraft:pressure_chamber_enchanting",  # no recipe
        "pneumaticcraft:fluid",  # fluid based, maybe later
        "pneumaticcraft:thermo_plant",  # fluid based, maybe later
        "pneumaticcraft:refinery",  # maybe later
        "pneumaticcraft:one_probe_helmet_crafting",
    ]

    for t in typesToIgnore:
        if t == Type:
            return None
    if Type == "pneumaticcraft:crafting_shaped_no_mirror":
        return __ConvertPneumaticCraftCraftingShapedNoMirrorRecipe(recipeToConvert, Type)
    elif Type == "pneumaticcraft:crafting_shaped_pressurizable":
        return __ConvertPneumaticCraftCraftingShapedNoMirrorRecipe(recipeToConvert, Type)
    elif Type == "pneumaticcraft:pressure_chamber":
        return __ConvertPneumaticCraftPressureChamberRecipe(recipeToConvert, Type)
    elif Type == "pneumaticcraft:compressor_upgrade_crafting":
        return __ConvertPneumaticCraftCraftingShapedNoMirrorRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None


# used for testing conversions
# testRecipeFileName = "pneumaticcraft_pressure_chamber"
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    Type, converted = ConvertPneumaticCraftRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
# testOutput = json.dumps(convertedRecipes, indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
