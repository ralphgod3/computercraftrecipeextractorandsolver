import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def ConvertModularRoutersRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "modularrouters:breaker_module":
        return None
    elif Type == "modularrouters:extruder_module_1":
        return None
    elif Type == "modularrouters:guide_book":
        return None
    elif Type == "modularrouters:module_reset":
        return None
    utils.Print("no recipe converter for " + Type)
    return None
