import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertTemplateBlastingRecipe(recipeToConvert, recipeType):
    if type(recipeToConvert["result"]) is dict:
        Type, name, count, nbt = converterUtils.convertItemToInfo(
            recipeToConvert["result"])
    else:
        Type = "item"
        name = recipeToConvert["result"]
        count = 1
        nbt = None
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    if type(recipeToConvert["ingredient"]) is list:
        for ing in recipeToConvert["ingredient"]:
            recipe = converterUtils.ItemRecipeRecipe()
            recipe.SetOutputCount(count)
            recipe.SetType(recipeType)
            recipe.AddSlotInRecipe(1, ing)
            outRecipe.AddRecipe(recipe)
    else:
        recipe = converterUtils.ItemRecipeRecipe()
        recipe.SetOutputCount(count)
        recipe.AddSlotInRecipe(1, recipeToConvert["ingredient"])
        recipe.SetType(recipeType)
        outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def __ConvertTemplateShapedCraftingRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = converterUtils.convertItemToInfo(
        recipeToConvert["result"])
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def __ConvertTemplateShapelessCraftingRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = converterUtils.convertItemToInfo(
        recipeToConvert["result"])
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def ConvertTemplateRecipe(recipeToConvert):
    typesToIgnore = [
        "template:templateRecipe",
    ]

    Type = recipeToConvert["type"]
    # check against non automateable types
    for t in typesToIgnore:
        if t == Type:
            return None
    if Type == "template:crafting_shaped":
        return __ConvertTemplateShapedCraftingRecipe(recipeToConvert, Type)
    elif Type == "template:crafting_shapeless":
        return __ConvertTemplateShapelessCraftingRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None


# used for testing conversions
# testRecipeFileName = "minecraft_smoking"
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    Type, converted = ConvertMinecraftRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
# testOutput = json.dumps(convertedRecipes, indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
