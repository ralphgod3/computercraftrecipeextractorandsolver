import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def ConvertTinkersConstructRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "tconstruct:ageable_severing": #not easily convertable
        return None
    elif Type == "tconstruct:alloy": #maybe later
        return None
    elif Type == "tconstruct:basin_filling": #maybe later
        return None
    elif Type == "tconstruct:casting_basin": #maybe later
        return None
    elif Type == "tconstruct:casting_table": #maybe later
        return None
    elif Type == "tconstruct:crafting_table_repair": #no recipe
        return None
    elif Type == "tconstruct:creative_slot_modifier": #no recipe
        return None
    elif Type == "tconstruct:damagable_melting": #no recipe
        return None
    elif Type == "tconstruct:entity_melting": #no recipe
        return None
    elif Type == "tconstruct:incremental_modifier": #no recipe
        return None
    elif Type == "tconstruct:incremental_modifier_salvage": #no recipe
        return None
    elif Type == "tconstruct:material": #no recipe
        return None
    elif Type == "tconstruct:material_fluid": #not easily convertable
        return None
    elif Type == "tconstruct:material_melting": #maybe later
        return None
    elif Type == "tconstruct:melting": #maybe later
        return None
    elif Type == "tconstruct:melting_fuel": #no recipe
        return None
    elif Type == "tconstruct:modifier": #not automatable
        return None
    elif Type == "tconstruct:modifier_salvage": #no recipe
        return None
    elif Type == "tconstruct:molding_table": #no clue what this is
        return None
    elif Type == "tconstruct:mooshroom_demushrooming": #no recipe
        return None
    elif Type == "tconstruct:ore_melting": #maybe later
        return None
    elif Type == "tconstruct:overslime_modifier": #no recipe
        return None
    elif Type == "tconstruct:part_builder": #not automatble
        return None
    elif Type == "tconstruct:player_beheading": #no recipe
        return None
    elif Type == "tconstruct:remove_modifier": #no recipe
        return None
    elif Type == "tconstruct:severing": #no recipe
        return None
    elif Type == "tconstruct:sheep_shearing": #no recipe
        return None
    elif Type == "tconstruct:snow_golem_beheading": #no recipe
        return None
    elif Type == "tconstruct:specialized_repair_kit": #no recipe
        return None
    elif Type == "tconstruct:specialized_station_repair": #no recipe
        return None
    elif Type == "tconstruct:table_casting_composite": #maybe later
        return None
    elif Type == "tconstruct:table_casting_material": #maybe later
        return None
    elif Type == "tconstruct:table_filling": #maybe later
        return None
    elif Type == "tconstruct:tinker_station_damaging": #no recipe
        return None
    elif Type == "tconstruct:tinker_station_part_swapping": #no recipe
        return None
    elif Type == "tconstruct:tinker_station_repair": #no recipe
        return None
    elif Type == "tconstruct:tool_building": #not convertable
        return None
    utils.Print("no recipe converter for " + Type)
    return None

##used for testing conversions
#testRecipeFileName = "simplybackpacks_backpack_upgrade"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertTinkersConstructRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()