import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertArsNouVeauBookUpgradeRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def __ConvertArsNouVeauEnchantingApparatusRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    if "count" in recipeToConvert["output"]:
        recipe.SetOutputCount(recipeToConvert["output"]["count"])
    slot = 1
    for i in range(20):
        item = "item_" + str(i)
        if item in recipeToConvert:
            recipe.AddSlotInRecipe(i, recipeToConvert[item][0])
            slot = i
    slot += 1
    for reagent in recipeToConvert["reagent"]:
        recipe.AddSlotInRecipe(slot, reagent)
        slot += 1

    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    return outRecipe.PrepareForSerialization()


def __ConvertArsNouVeauGlyphRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    input = {}
    input["item"] = recipeToConvert["input"]
    recipe.AddSlotInRecipe(1, input)
    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    output = {}
    output["item"] = recipeToConvert["output"]
    outRecipe.SetOutputNameAndType(output)
    return outRecipe.PrepareForSerialization()


def __ConvertArsNouVeauPotionFlaskRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(
        recipeToConvert)
    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()


def ConvertArsNouVeauRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    typesToIgnore = [
        "ars_nouveau:dye",
        "ars_nouveau:enchantment",
        "ars_nouveau:glyph",
        "ars_nouveau:crush",
        "ars_nouveau:imbuement",
        "ars_nouveau:reactive_enchantment",
        "ars_nouveau:spell_write",
        "ars_nouveau:summon_ritual",
        "ars_nouveau:caster_tome",
        "ars_nouveau:armor_upgrade",
    ]

    for t in typesToIgnore:
        if t == Type:
            return None

    if Type == "ars_nouveau:book_upgrade":
        return __ConvertArsNouVeauBookUpgradeRecipe(recipeToConvert, Type)
    elif Type == "ars_nouveau:enchanting_apparatus":
        return __ConvertArsNouVeauEnchantingApparatusRecipe(recipeToConvert, Type)
    elif Type == "ars_nouveau:glyph_recipe":
        return __ConvertArsNouVeauGlyphRecipe(recipeToConvert, Type)
    elif Type == "ars_nouveau:potion_flask":
        return __ConvertArsNouVeauPotionFlaskRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None


# used for testing convertes
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\ars_nouveau_potion_flask.json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    _, converted = ConvertArsNouVeauRecipe(recipe["recipe"])
#    convertedRecipes.append(converted)
#
#
#
#
#
# testOutput = json.dumps(convertedRecipes, indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
