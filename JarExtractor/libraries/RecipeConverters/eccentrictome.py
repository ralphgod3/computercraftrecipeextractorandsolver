import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def ConvertEccentrictomeRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    typesToIgnore = [
        "eccentrictome:attachment",
    ]
    for t in typesToIgnore:
        if t == Type:
            return None
    utils.Print("no recipe converter for " + Type)
    return None


# used for testing convertes
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\appliedenergistics2_inscriber.json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    _, converted = ConvertAE2Recipe(recipe["recipe"])
#    convertedRecipes.append(converted)
#
#
#
#
#
# testOutput = json.dumps(convertedRecipes,indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
