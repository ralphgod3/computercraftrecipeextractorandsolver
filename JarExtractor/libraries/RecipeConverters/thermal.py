import json
import os
import math
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertThermalPulverizerRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    isFirstOutput = True
    for result in recipeToConvert["result"]:
        count = 1
        Type, name, _, nbt = converterUtils.convertItemToInfo(result)
        if "chance" in result:
            count = math.floor(result["chance"])
            if count < 1:
                count = 1
        if isFirstOutput == True:
            outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
            recipe.SetOutputCount(count)
            isFirstOutput = False
            continue
        recipe.AddOptionalOutputManually(Type, name, count, nbt)
    count = 1
    # todo: generate multiple recipes when multi items are found
    if "value" in recipeToConvert["ingredient"]:
        toConvert = recipeToConvert["ingredient"]["value"][0]
    else:
        toConvert = recipeToConvert["ingredient"]
    if "count" in recipeToConvert["ingredient"]:
        count = recipeToConvert["ingredient"]["count"]
    Type, name, _, nbt = converterUtils.convertItemToInfo(toConvert)
    recipe.AddSlotInRecipeManually(1, Type, name, count, nbt)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def __ConvertThermalSawmillRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    isFirstOutput = True
    for result in recipeToConvert["result"]:
        count = 1
        Type, name, _, nbt = converterUtils.convertItemToInfo(result)
        if "chance" in result:
            count = math.floor(result["chance"])
        if "count" in result:
            count = math.floor(result["count"])
        if count < 1:
            count = 1
        if isFirstOutput == True:
            outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
            recipe.SetOutputCount(count)
            isFirstOutput = False
            continue
        recipe.AddOptionalOutputManually(Type, name, count, nbt)
    count = 1
    # todo: generate multiple recipes when multi items are found
    if "value" in recipeToConvert["ingredient"]:
        toConvert = recipeToConvert["ingredient"]["value"][0]
    else:
        toConvert = recipeToConvert["ingredient"]
    if "count" in recipeToConvert["ingredient"]:
        count = recipeToConvert["ingredient"]["count"]
    Type, name, _, nbt = converterUtils.convertItemToInfo(toConvert)
    recipe.AddSlotInRecipeManually(1, Type, name, count, nbt)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def __ConvertThermalSmelterRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetType(recipeType)
    isFirstOutput = True
    for result in recipeToConvert["result"]:
        count = 1
        Type, name, _, nbt = converterUtils.convertItemToInfo(result)
        if "chance" in result:
            count = math.floor(result["chance"])
        if "count" in result:
            count = math.floor(result["count"])
        if count < 1:
            count = 1
        if isFirstOutput == True:
            outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
            recipe.SetOutputCount(count)
            isFirstOutput = False
            continue
        recipe.AddOptionalOutputManually(Type, name, count, nbt)

    # todo: generate multiple recipes when multi items are found
    slot = 1
    if "input" in recipeToConvert:
        for ingredient in recipeToConvert["input"]:
            count = 1
            if "value" in ingredient:
                toConvert = ingredient["value"][0]
            else:
                toConvert = ingredient
            if "count" in ingredient:
                count = ingredient["count"]
            Type, name, _, nbt = converterUtils.convertItemToInfo(toConvert)
            recipe.AddSlotInRecipeManually(slot, Type, name, count, nbt)
            slot += 1
        outRecipe.AddRecipe(recipe)
    elif "ingredient" in recipeToConvert:
        count = 1
        if "value" in recipeToConvert["ingredient"]:
            toConvert = recipeToConvert["ingredient"]["value"][0]
        else:
            toConvert = recipeToConvert["ingredient"]
        if "count" in recipeToConvert["ingredient"]:
            count = recipeToConvert["ingredient"]["count"]
        Type, name, _, nbt = converterUtils.convertItemToInfo(toConvert)
        recipe.AddSlotInRecipeManually(slot, Type, name, count, nbt)
        outRecipe.AddRecipe(recipe)
    elif "ingredients" in recipeToConvert:
        for ing in recipeToConvert["ingredients"]:
            if "value" in ing:
                ing = ing["value"][0]
            recipe.AddSlotInRecipe(slot, ing)
            slot += 1
        outRecipe.AddRecipe(recipe)
    else:
        raise NotImplementedError("recipe converter thermal:smelter cant parse recipe")

    return outRecipe.PrepareForSerialization()


def ConvertThermalRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    typesToIgnore = [
        "thermal",  # why the fuck does this exist as a recipe
        "thermal:bottler",  # fluid crafting
        "thermal:centrifuge",  # partial fluid crafting, maybe later
        "thermal:chiller",  # fluid crafting
        "thermal:compression_fuel",  # no recipe
        "thermal:crucible",  # fluid crafting
        "thermal:furnace",  # only 1 recipe for rotten flesh
        "thermal:insolator",  # chance based recipes
        "thermal:insolator_catalyst",  # not a recipe
        "thermal:lapidary_fuel",  # not a recipe
        "thermal:magmatic_fuel",  # not a recipe
        "thermal:numismatic_fuel",  # not a recipe
        "thermal:potion_diffuser_boost",  # not a recipe
        "thermal:press",  # fluid crafting an return items not specified in recipe, maybe later
        "thermal:pulverizer_catalyst",  # not a recipe
        "thermal:pyrolyzer",  # fluid crafting
        "thermal:refinery",  # fluid crafting
        "thermal:rock_gen",  # not a recipe
        "thermal:smelter_catalyst",  # not a recipe
        "thermal:stirling_fuel",  # not a recipe
        "thermal:tree_extractor",  # fluid crafting and partially a recipe
        "thermal:tree_extractor_boost",  # not a recipe
    ]
    for t in typesToIgnore:
        if t == Type:
            return None

    if Type == "thermal:pulverizer":
        return __ConvertThermalPulverizerRecipe(recipeToConvert, Type)
    elif Type == "thermal:sawmill":
        return __ConvertThermalSawmillRecipe(recipeToConvert, Type)
    elif Type == "thermal:smelter":  # induction smelter
        return __ConvertThermalSmelterRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None

# used for testing conversions
# testRecipeFileName = "thermal_smelter"
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    Type, converted = ConvertThermalRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
# testOutput = json.dumps(convertedRecipes, indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
