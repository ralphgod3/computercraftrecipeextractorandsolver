import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertEnergizedPowerChargerRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    recipe = converterUtils.ItemRecipeRecipe()
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    if not "count" in recipeToConvert:
        recipe.SetOutputCount(1)
    else:
        recipe.SetOutputCount(recipeToConvert["count"])
    recipe.AddSlotInRecipe(1, recipeToConvert["ingredient"])
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def __ConvertEnergizedPowerCompressorRecipe(recipeToConvert, recipeType):
    return __ConvertEnergizedPowerChargerRecipe(recipeToConvert, recipeType)


def __ConvertEnergizedPowerCrusherRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    outCount = 1
    if "count" in recipeToConvert["output"]:
        outCount = recipeToConvert["output"]["count"]
    if isinstance(recipeToConvert["ingredient"], list):
        for ingredient in recipeToConvert["ingredient"]:
            recipe = converterUtils.ItemRecipeRecipe()
            recipe.AddSlotInRecipe(1, ingredient)
            recipe.SetType(recipeType)

            recipe.SetOutputCount(outCount)
            outRecipe.AddRecipe(recipe)
    else:
        recipe = converterUtils.ItemRecipeRecipe()
        recipe.AddSlotInRecipe(1, recipeToConvert["ingredient"])
        recipe.SetType(recipeType)
        recipe.SetOutputCount(outCount)
        outRecipe.AddRecipe(recipe)

    return outRecipe.PrepareForSerialization()


def __ConvertEnergizedPowerEnergizerRecipe(recipeToConvert, recipeType):
    return __ConvertEnergizedPowerChargerRecipe(recipeToConvert, recipeType)


def __ConvertEnergizedPowerSawmillRecipe(recipeToConvert, recipeType):
    return __ConvertEnergizedPowerCrusherRecipe(recipeToConvert, recipeType)


def ConvertEnergizedPowerRecipe(recipeToConvert):
    typesToIgnore = [
        "energizedpower:plant_growth_chamber",  # not dealing with plants
        "energizedpower:plant_growth_chamber_fertilizer",  # only modifiers no recipe
    ]

    Type = recipeToConvert["type"]
    # check against non automateable types
    for t in typesToIgnore:
        if t == Type:
            return None
    if Type == "energizedpower:charger":
        return __ConvertEnergizedPowerChargerRecipe(recipeToConvert, Type)
    elif Type == "energizedpower:compressor":
        return __ConvertEnergizedPowerCompressorRecipe(recipeToConvert, Type)
    elif Type == "energizedpower:crusher":
        return __ConvertEnergizedPowerCrusherRecipe(recipeToConvert, Type)
    elif Type == "energizedpower:energizer":
        return __ConvertEnergizedPowerEnergizerRecipe(recipeToConvert, Type)
    elif Type == "energizedpower:sawmill":
        return __ConvertEnergizedPowerSawmillRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None


# used for testing conversions
# testRecipeFileName = "minecraft_smoking"
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    Type, converted = ConvertMinecraftRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
# testOutput = json.dumps(convertedRecipes, indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
