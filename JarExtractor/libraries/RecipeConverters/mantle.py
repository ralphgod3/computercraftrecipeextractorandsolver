import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def ConvertMantleRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "mantle:crafting_shaped_retextured":
        return None
    utils.Print("no recipe converter for " + Type)
    return None