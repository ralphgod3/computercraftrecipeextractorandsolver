import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def ConvertChisselAndBitsRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "chiselsandbits:bag_dyeing":
        return None
    elif Type == "chiselsandbits:bit_saw_crafting":
        return None
    elif Type == "chiselsandbits:chisel_block_crafting":
        return None
    elif Type == "chiselsandbits:chisel_crafting":
        return None
    elif Type == "chiselsandbits:mirror_transfer_crafting":
        return None
    elif Type == "chiselsandbits:negative_inversion_crafting":
        return None
    elif Type == "chiselsandbits:stackable_crafting":
        return None
    
    utils.Print("no recipe converter for " + Type)
    return None




#used for testing conversions
#testRecipeFileName = "botania_gog_alternation"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertChisselAndBitsRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()