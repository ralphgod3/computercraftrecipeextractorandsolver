import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def __ConverXreliquaryMobCharmTRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = converterUtils.convertItemToInfo(recipeToConvert["result"])
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def ConvertXreliquaryRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "xreliquary:mob_charm":
        return __ConverXreliquaryMobCharmTRecipe(recipeToConvert, Type)
    elif Type == "xreliquary:fragment_to_spawn_egg":
        return None
    elif Type == "xreliquary:potion_effects":
        return None
    elif Type == "xreliquary:alkahestry_drain":
        return None
    elif Type == "xreliquary:alkahestry_crafting":
        return None
    elif Type == "xreliquary:alkahestry_charging":
        return None
    utils.Print("no recipe converter for " + Type)
    return None

##used for testing conversions
#testRecipeFileName = "wormhole_nbtrecipe"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertWormholeRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()