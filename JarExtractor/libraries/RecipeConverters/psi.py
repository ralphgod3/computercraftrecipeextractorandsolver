import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def __ConvertPsiBulletUpgradeRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = converterUtils.convertItemToInfo(recipeToConvert["result"])
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def ConvertPsiRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "psi:bullet_to_drive": #no recipe
        return None
    elif Type == "psi:bullet_upgrade":
        return __ConvertPsiBulletUpgradeRecipe(recipeToConvert, Type)
    elif Type == "psi:colorizer_change": #no recipe
        return None
    elif Type == "psi:dimension_trick_crafting": #no recipe
        return None
    elif Type == "psi:drive_duplicate": #no recipe
        return None
    elif Type == "psi:scavenge": #no recipe
        return None
    elif Type == "psi:sensor_attach": #no recipe
        return None
    elif Type == "psi:sensor_remove": #no recipe
        return None
    elif Type == "psi:trick_crafting": #not automatable i think
        return None
    
    utils.Print("no recipe converter for " + Type)
    return None



##used for testing conversions
#testRecipeFileName = "psi_bullet_upgrade"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertPsiRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()