import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def __ConvertBloodMagicAlchemyTableRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()

    slot = 1
    for input in recipeToConvert["input"]:
        #todo: add seperate recipe with alternative item
        if type(input) is list: 
            recipe.AddSlotInRecipe(slot, input[1])
        else:
            recipe.AddSlotInRecipe(slot, input)
        slot+=1
    _,_, count,_ = converterUtils.convertItemToInfo(recipeToConvert["output"])
    recipe.SetOutputCount(count)

    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    return outRecipe.PrepareForSerialization()

def __ConvertBloodMagicAltarRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.AddSlotInRecipe(1, recipeToConvert["input"])
    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    return outRecipe.PrepareForSerialization()

def __ConvertBloodMagicArrayRecipe(recipeToConvert, recipeType):
    _, outName, _, _ = converterUtils.convertItemToInfo(recipeToConvert["output"])
    #special case for bloodmagic alchemy array buff recipes -_-
    if outName == "minecraft:air":
        return None
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.AddSlotInRecipe(1, recipeToConvert["baseinput"])
    recipe.AddSlotInRecipe(2, recipeToConvert["addedinput"])
    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    return outRecipe.PrepareForSerialization()

def __ConvertBloodMagicSouldForgeRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    for i in range(20):
        if "input" + str(i) in recipeToConvert:
            #todo: handle recipes with alternative inputs better
            if type(recipeToConvert["input" + str(i)]) is list:
                recipe.AddSlotInRecipe(i+1, recipeToConvert["input" + str(i)][1])
            else:
                recipe.AddSlotInRecipe(i+1, recipeToConvert["input" + str(i)])
    outRecipe = converterUtils.ItemRecipe()
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["output"])
    return outRecipe.PrepareForSerialization()


def ConvertBloodMagicRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "bloodmagic:alchemytable":
        return __ConvertBloodMagicAlchemyTableRecipe(recipeToConvert, Type)
    elif Type == "bloodmagic:altar":
        return __ConvertBloodMagicAltarRecipe(recipeToConvert, Type)
    elif Type == "bloodmagic:arc":
        return None # needs player interaction not automatable atm
    elif Type == "bloodmagic:array":
        #blood magic recipes are fucking retarded
        return __ConvertBloodMagicArrayRecipe(recipeToConvert, Type)
    elif Type == "bloodmagic:soulforge":
        return __ConvertBloodMagicSouldForgeRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None




#used for testing convertes
#testRecipeFileName = "bloodmagic_soulforge"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertBloodMagicRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()