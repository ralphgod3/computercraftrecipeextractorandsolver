#not a mod but handles shaped and shapeless recipes of type crafting since apparently minecraft allows people to leave out the minecraft: part of the handler
#and worse some people actually do that

import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

def __ConvertCraftingShapedRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(recipeToConvert)
    _,_,count,_ = converterUtils.convertItemToInfo(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    recipe.SetType(converterUtils.craftingHandler)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()

def __ConvertCraftingShapelessRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertUnshapedPatternToSlotsInRecipe(recipeToConvert)
    _,_,count,_ = converterUtils.convertItemToInfo(recipeToConvert["result"])
    recipe.SetType(converterUtils.craftingHandler)
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndType(recipeToConvert["result"])
    return outRecipe.PrepareForSerialization()

def ConvertCraftingRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "crafting_shaped":
        return __ConvertCraftingShapedRecipe(recipeToConvert, Type)
    elif Type == "crafting_shapeless":
        return __ConvertCraftingShapelessRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None




##used for testing conversions
#testRecipeFileName = "crafting_shapeless"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertCraftingRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()