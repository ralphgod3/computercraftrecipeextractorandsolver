import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils

#mekanism does the same stupid thing as immersive engineering so well convert it to a normal resource
def __ConvertMekanismIngredientToStandardFormat(ingredient, countTagName = "count", nbtTagName = "nbt"):
    count = 1
    if countTagName in ingredient:
        count = ingredient[countTagName]
    if "ingredient" in ingredient:
        ingredient = ingredient["ingredient"]
    if countTagName in ingredient:
        count = ingredient[countTagName]
    Type, name, _, nbt = converterUtils.convertItemToInfo(ingredient, nbtTagName)
    return Type, name, count, nbt

def __ConvertMekanismCombiningRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ItemRecipeRecipe()
    Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(recipeToConvert["mainInput"], "amount")
    recipe.AddSlotInRecipeManually(1, Type, name, count, nbt)
    Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(recipeToConvert["extraInput"], "amount")
    recipe.AddSlotInRecipeManually(2, Type, name, count, nbt)
    Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(recipeToConvert["output"], "amount")
    recipe.SetType(recipeType)
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    return outRecipe.PrepareForSerialization()

def __ConvertMekanismCrushingRecipe(recipeToConvert, recipeType):
    Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(recipeToConvert["output"])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    if type(recipeToConvert["input"]) is list:
        for inputs in recipeToConvert["input"]:
            recipe = converterUtils.ItemRecipeRecipe()
            recipe.SetOutputCount(count)
            Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(inputs)
            recipe.AddSlotInRecipeManually(1, Type, name, count, nbt)
            recipe.SetType(recipeType)
            outRecipe.AddRecipe(recipe)
    if type(recipeToConvert["input"]["ingredient"]) is list:
        for inputs in recipeToConvert["input"]["ingredient"]:
            recipe = converterUtils.ItemRecipeRecipe()
            recipe.SetOutputCount(count)
            Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(inputs)
            recipe.AddSlotInRecipeManually(1, Type, name, count, nbt)
            recipe.SetType(recipeType)
            outRecipe.AddRecipe(recipe)
    else:
        recipe = converterUtils.ItemRecipeRecipe()
        recipe.SetOutputCount(count)
        recipe.SetType(recipeType)
        Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(recipeToConvert["input"])
        recipe.AddSlotInRecipeManually(1, Type, name, count, nbt)
        outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()

def __ConvertMekanismEnrichingRecipe(recipeToConvert, recipeType):
    return __ConvertMekanismCrushingRecipe(recipeToConvert, "mekanism:enriching")

def __ConvertMekanismDataRecipe(recipeToConvert, recipeType):
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(recipeToConvert["result"])
    recipe.SetOutputCount(count)
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.AddRecipe(recipe)
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    return outRecipe.PrepareForSerialization()

def __ConvertMekanismSawingRecipe(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(recipeToConvert["mainOutput"])
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    if type(recipeToConvert["input"]) is list:
        for inputs in recipeToConvert["input"]:
            recipe = converterUtils.ItemRecipeRecipe()
            recipe.SetType(recipeType)
            recipe.SetOutputCount(count)
            Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(inputs)
            recipe.AddSlotInRecipeManually(1, Type, name, count, nbt)
            if "secondaryOutput" in recipeToConvert:
                Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(recipeToConvert["secondaryOutput"])
                recipe.AddOptionalOutputManually(Type, name, count, nbt)
            outRecipe.AddRecipe(recipe)
    else:
        recipe = converterUtils.ItemRecipeRecipe()
        recipe.SetType(recipeType)
        recipe.SetOutputCount(count)
        Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(recipeToConvert["input"])
        recipe.AddSlotInRecipeManually(1, Type, name, count, nbt)
        if "secondaryOutput" in recipeToConvert:
            Type, name, count, nbt = __ConvertMekanismIngredientToStandardFormat(recipeToConvert["secondaryOutput"])
            recipe.AddOptionalOutputManually(Type, name, count, nbt)
        outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()
        

def ConvertMekanismRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    if Type == "mekanism:activating": #gas recipe
        return None
    elif Type == "mekanism:bin_extract": #not a recipe
        return None
    elif Type == "mekanism:bin_insert": #not a recipe
        return None
    elif Type == "mekanism:centrifuging": #gas recipe
        return None
    elif Type == "mekanism:chemical_infusing": #gas recipe
        return None
    elif Type == "mekanism:combining":
        return __ConvertMekanismCombiningRecipe(recipeToConvert, Type)
    elif Type == "mekanism:compressing": #gas recipe
        return None
    elif Type == "mekanism:crushing":
        return __ConvertMekanismCrushingRecipe(recipeToConvert, Type)
    elif Type == "mekanism:crystallizing": #gas recipe
        return None
    elif Type == "mekanism:dissolution": #slurry recipe
        return None
    elif Type == "mekanism:energy_conversion": #not a recipe
        return None
    elif Type == "mekanism:enriching":
        return __ConvertMekanismEnrichingRecipe(recipeToConvert, Type)
    elif Type == "mekanism:evaporating": #not something you would want to automate
        return None
    elif Type == "mekanism:gas_conversion": #add maybe later ?
        return None
    elif Type == "mekanism:infusion_conversion": #not a recipe, deal with in some way to modify enriching recipes
        return None
    elif Type == "mekanism:injecting": #gas recipes
        return None
    elif Type == "mekanism:mek_data": #add
        return __ConvertMekanismDataRecipe(recipeToConvert, Type)
    elif Type == "mekanism:metallurgic_infusing": #add, needs special logic to convert the infusion conversion recipes
        return None
    elif Type == "mekanism:nucleosynthesizing": #maybe later
        return None
    elif Type == "mekanism:oxidizing": #maybe later
        return None
    elif Type == "mekanism:purifying": #gas recipe
        return None
    elif Type == "mekanism:reaction": #reactor recipes?
        return None
    elif Type == "mekanism:rotary": #maybe later
        return None
    elif Type == "mekanism:sawing": #add
        return __ConvertMekanismSawingRecipe(recipeToConvert, Type)
    elif Type == "mekanism:separating": #maybe later
        return None
    elif Type == "mekanism:washing": #maybe later
        return None
    elif Type == "mekanism:banner_shield": #not a recipe
        return None
    
    utils.Print("no recipe converter for " + Type)
    return None



##used for testing conversions
#testRecipeFileName = "mekanism_sawing"
#cwd = os.path.dirname(os.path.realpath(__file__))
#cwd = cwd[0: cwd.rfind("\\")]
#cwd = cwd[0: cwd.rfind("\\")]
#path = cwd + "\\output\\recipesByType\\"+ testRecipeFileName +".json"
#file = open(path,"r")
#
#js = json.load(file)
#file.close()
#
#convertedRecipes = []
#
#for recipe in js:
#    Type, converted = ConvertMekanismRecipe(recipe["recipe"])
#    if Type != None:
#        convertedRecipes.append(converted)
#
#testOutput = json.dumps(convertedRecipes, indent=3)
#file = open(cwd + "\\" + "test.json", "w")
#file.write(testOutput)
#file.close()