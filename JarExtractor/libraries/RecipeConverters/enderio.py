import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertEnderIoAlloySmeltingRecipe(recipeToConvert, recipeType):
    Type, name, count, nbt = converterUtils.convertItemToInfo(
        recipeToConvert["result"])
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetOutputCount(count)
    recipe.SetType(recipeType)
    slot = 1

    for ing in recipeToConvert["inputs"]:
        # todo: generate multiple recipes when required instead of picking first valid one
        print(type(ing["ingredient"]))
        if isinstance(ing["ingredient"], list):
            ing["ingredient"] = ing["ingredient"][0]
        itemType, name, count, nbt = converterUtils.convertItemToInfo(ing["ingredient"])
        recipe.AddSlotInRecipeManually(slot, itemType, name, count, nbt)
        slot += 1
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def __ConvertEnderIoSagMillingRecipe(recipeToConvert, recipeType):
    hasOnlyChanceItems = True
    outRecipe = converterUtils.ItemRecipe()
    recipe = converterUtils.ItemRecipeRecipe()
    for output in recipeToConvert["outputs"]:
        if "count" in output:
            hasOnlyChanceItems = False
            itemType, itemName, itemCount, itemNbt = converterUtils.convertItemToInfo(
                output)
            if not outRecipe.OutputIsSet():
                outRecipe.SetOutputNameAndTypeManually(
                    itemType, itemName, itemNbt)
                recipe.SetOutputCount(itemCount)
            else:
                itemType, itemName, itemCount, itemNbt = converterUtils.convertItemToInfo(
                    output)
                recipe.AddOptionalOutputManually(
                    itemType, itemName, itemCount, itemNbt)
        else:
            itemType, itemName, itemCount, itemNbt = converterUtils.convertItemToInfo(
                output)
            if "chance" in output:
                itemCount = output["chance"]
            recipe.AddOptionalOutputManually(
                itemType, itemName, itemCount, itemNbt)
    # autocrafting will not deal with chance items, too messy
    if hasOnlyChanceItems:
        return None
    recipe.AddSlotInRecipe(1, recipeToConvert["input"])
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def __ConvertEnderIoSlicingRecipe(recipeToConvert, recipeType):
    Type = "item"
    name = recipeToConvert["output"]
    count = 1
    nbt = None
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)

    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetOutputCount(count)
    recipe.SetType(recipeType)
    slot = 1
    for ing in recipeToConvert["inputs"]:

        recipe.AddSlotInRecipe(slot, ing)
        slot += 1
    outRecipe.AddRecipe(recipe)

    return outRecipe.PrepareForSerialization()


def ConvertEnderioRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    typesToIgnore = [
        "enderio:dark_steel_upgrade",  # no recipes
        "enderio:enchanting",  # not automateable
        "enderio:fire_crafting",  # not automateable
        "enderio:grinding_ball",  # does not contain recipes
        "enderio:painting",  # not really automatable since all colours have same end product
        "enderio:shaped_entity_storage",  # has to be done manually to get spawner you want
        "enderio:soul_binding",  # cant deal with entity types atm
        "enderio:tank",  # in world crafting using deployer dont see a use at the moment for this
    ]
    for t in typesToIgnore:
        if t == Type:
            return None
    if Type == "enderio:alloy_smelting":
        return __ConvertEnderIoAlloySmeltingRecipe(recipeToConvert, Type)
    elif Type == "enderio:sagmilling":
        return __ConvertEnderIoSagMillingRecipe(recipeToConvert, Type)
    elif Type == "enderio:slicing":
        return __ConvertEnderIoSlicingRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None


# used for testing converters
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\appliedenergistics2_inscriber.json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    _, converted = ConvertAE2Recipe(recipe["recipe"])
#    convertedRecipes.append(converted)
#
#
#
#
#
# testOutput = json.dumps(convertedRecipes,indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
