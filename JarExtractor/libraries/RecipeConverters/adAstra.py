import json
import os
import libraries.RecipeConverters.converterUtils as converterUtils
import libraries.utils as utils


def __ConvertAdAstraShapedSpacesuitCrafting(recipeToConvert, recipeType):
    outRecipe = converterUtils.ItemRecipe()
    Type, name, count, nbt = converterUtils.convertItemToInfo(
        recipeToConvert["result"])
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    recipe = converterUtils.ConvertShapedPatternToSlotsInRecipe(
        recipeToConvert)
    recipe.SetType(converterUtils.craftingHandler)
    recipe.SetOutputCount(count)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def __ConvertAdAstraCompressingRecipe(recipeToConvert, recipeType):
    Type = "item"
    name = recipeToConvert["output"]["id"]
    count = recipeToConvert["output"]["count"]
    nbt = None
    outRecipe = converterUtils.ItemRecipe()
    outRecipe.SetOutputNameAndTypeManually(Type, name, nbt)
    recipe = converterUtils.ItemRecipeRecipe()
    recipe.SetOutputCount(count)
    recipe.AddSlotInRecipe(1, recipeToConvert["input"])
    recipe.SetType(recipeType)
    outRecipe.AddRecipe(recipe)
    return outRecipe.PrepareForSerialization()


def ConvertAdAstraRecipe(recipeToConvert):
    Type = recipeToConvert["type"]
    typesToIgnore = [
        "ad_astra:nasa_workbench",
        "ad_astra:space_station",
        "ad_astra:fuel_conversion",
        "ad_astra:cryo_fuel_conversion",
        "ad_astra:oxygen_conversion",
        "ad_astra:hammering",
    ]
    for t in typesToIgnore:
        if t == Type:
            return None
    if Type == "ad_astra:crafting_shaped_space_suit":
        return __ConvertAdAstraShapedSpacesuitCrafting(recipeToConvert, Type)
    elif Type == "ad_astra:compressing":
        return __ConvertAdAstraCompressingRecipe(recipeToConvert, Type)
    utils.Print("no recipe converter for " + Type)
    return None


# used for testing converters
# cwd = os.path.dirname(os.path.realpath(__file__))
# cwd = cwd[0: cwd.rfind("\\")]
# cwd = cwd[0: cwd.rfind("\\")]
# path = cwd + "\\output\\recipesByType\\appliedenergistics2_inscriber.json"
# file = open(path,"r")
#
# js = json.load(file)
# file.close()
#
# convertedRecipes = []
#
# for recipe in js:
#    _, converted = ConvertAE2Recipe(recipe["recipe"])
#    convertedRecipes.append(converted)
#
#
#
#
#
# testOutput = json.dumps(convertedRecipes,indent=3)
# file = open(cwd + "\\" + "test.json", "w")
# file.write(testOutput)
# file.close()
