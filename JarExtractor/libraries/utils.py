import json
import zipfile
import os
import toml


logs = []


def Print(text):
    logs.append(text + "\n")
    print(text)


def getLogs():
    return logs


def ItemNeedsToBeAddedToItems(items, item):
    if item["type"] != "item":
        return False
    for i in items:
        if i["name"] == item["name"]:
            if "nbtUnhashed" in i and "nbtUnhashed" in item and i["nbtUnhashed"] == item["nbtUnhashed"]:
                return False
            elif "nbtUnhashed" not in i and "nbtUnhashed" not in item:
                return False
    return True


def CreateItemEntryForItem(item):
    entry = {}
    entry["name"] = item["name"]
    if "nbtUnhashed" in item:
        entry["nbtUnhashed"] = item["nbtUnhashed"]
    return entry


def SaveJsonFile(filePath, objToSerialize):
    if not os.path.exists(filePath[0: filePath.rfind("\\")]):
        os.makedirs(filePath[0: filePath.rfind("\\")])
    fout = open(filePath, "w")
    serialized = json.dumps(objToSerialize)
    fout.write(serialized)
    fout.close()


def GetJarRecipes(filePath):
    """gets all recipes as a list of dictionaries from a jar file
    Args:
        filePath (string): absolute jar file location to get recipes for
    Returns:
        list: list of dictionaries with each recipe from the jar file in it
    """
    toReturn = []
    zfile = zipfile.ZipFile(filePath, "r")
    for info in zfile.infolist():
        fname = info.filename
        if fname.startswith("data/") and fname.endswith(".json") and "recipes" in fname and not ("loot_table" in fname or "advancements" in fname):
            entry = {}
            entry["jarName"] = filePath[filePath.rfind("\\")+1:]
            tempMod = fname.replace("data/", "")
            entry["mod"] = tempMod[0: tempMod.find("/")]
            entry["recipe"] = json.loads(zfile.read(fname))
            toReturn.append(entry)
    zfile.close()
    return toReturn


def GetItemDataFromJar(filePath):
    toReturn = []
    zfile = zipfile.ZipFile(filePath, "r")
    for info in zfile.infolist():
        fname = info.filename
        if fname.startswith("data/") and fname.endswith(".json") and "tags" in fname and not ("loot_table" in fname or "advancements" in fname or "recipes" in fname or "whitelisted" in fname or "blacklisted" in fname):
            splitPath = fname.split("/")
            if len(splitPath) == 5:
                entry = {}
                entry["jarName"] = filePath[filePath.rfind("\\")+1:]
                entry["tagType"] = splitPath[3]
                entry["tagName"] = splitPath[1] + ":" + \
                    splitPath[4].replace(".json", "")
                entry["items"] = json.loads(zfile.read(fname))["values"]
                toReturn.append(entry)
    zfile.close()
    return toReturn


def SplitRecipesByType(recipes) -> dict[str, any]:
    """split recipes by type, used as intermediary output for debugging

    Args:
        recipes (output from GetRecipesFromJar): the output from the GetRecipesByJar function

    Returns:
        dict: dictionary with all recipes sorted into different crafting types
    """
    retTypes = {}
    for recipe in recipes:
        if "type" in recipe["recipe"]:
            if not recipe["recipe"]["type"] in retTypes:
                retTypes[recipe["recipe"]["type"]] = []
            retTypes[recipe["recipe"]["type"]].append(recipe)
        else:
            if not recipe["mod"] in retTypes:
                retTypes[recipe["mod"]] = []
            retTypes[recipe["mod"]].append(recipe)
    return retTypes


def ListFilesRecursively(filePath):
    output = []
    for root, _, files in os.walk(filePath):
        for file in files:
            output.append(root + "\\" + file)
    return output


def IsTomlFile(filePath):
    if not os.path.isfile(filePath):
        return False
    # check if it isnt a backup file
    if ".toml" in filePath and not ".bak" in filePath:
        return True
    return False


def GetTomlData(filePath):
    file = open(filePath, "r")
    tempToml = file.read()
    file.close()
    return toml.loads(tempToml)
