from libraries.RecipeConverters import adAstra
from libraries.RecipeConverters import appliedEnergistics2
from libraries.RecipeConverters import arsNouveau
from libraries.RecipeConverters import astralSorcery
from libraries.RecipeConverters import bloodMagic
from libraries.RecipeConverters import bookshelf
from libraries.RecipeConverters import botania
from libraries.RecipeConverters import botanyPots
from libraries.RecipeConverters import buildingGadgets
from libraries.RecipeConverters import car
from libraries.RecipeConverters import cataclysm
from libraries.RecipeConverters import chisselAndBits
from libraries.RecipeConverters import computercraft
from libraries.RecipeConverters import crafting
from libraries.RecipeConverters import create
from libraries.RecipeConverters import cyclic
from libraries.RecipeConverters import eccentrictome
from libraries.RecipeConverters import emojiful
from libraries.RecipeConverters import enderchests
from libraries.RecipeConverters import enderio
from libraries.RecipeConverters import enderStorage
from libraries.RecipeConverters import endertanks
from libraries.RecipeConverters import energizedpower
from libraries.RecipeConverters import everlastingabilities
from libraries.RecipeConverters import fluxNetworks
from libraries.RecipeConverters import immersiveEngineering
from libraries.RecipeConverters import industrialForegoing
from libraries.RecipeConverters import integratedDynamics
from libraries.RecipeConverters import ironfurnaces
from libraries.RecipeConverters import laserio
from libraries.RecipeConverters import manaAndArtifice
from libraries.RecipeConverters import mantle
from libraries.RecipeConverters import mcjtylib
from libraries.RecipeConverters import mekanism
from libraries.RecipeConverters import mineColonies
from libraries.RecipeConverters import minecraft
from libraries.RecipeConverters import modulargolems
from libraries.RecipeConverters import modularRouters
from libraries.RecipeConverters import mowlib
from libraries.RecipeConverters import mythicBotany
from libraries.RecipeConverters import naturesAura
from libraries.RecipeConverters import occultism
from libraries.RecipeConverters import oreberriesreplanted
from libraries.RecipeConverters import patchouli
from libraries.RecipeConverters import pedestals
from libraries.RecipeConverters import pipez
from libraries.RecipeConverters import planttech2
from libraries.RecipeConverters import pneumaticCraft
from libraries.RecipeConverters import pocketStorage
from libraries.RecipeConverters import portabletanks
from libraries.RecipeConverters import powah
from libraries.RecipeConverters import propellerhats
from libraries.RecipeConverters import psi
from libraries.RecipeConverters import rats
from libraries.RecipeConverters import rechiseled
from libraries.RecipeConverters import recipe as Recipe
from libraries.RecipeConverters import refinedstorage
from libraries.RecipeConverters import rftoolsDim
from libraries.RecipeConverters import rootsclassic
from libraries.RecipeConverters import sewingkit
from libraries.RecipeConverters import sfm
from libraries.RecipeConverters import shetiphiancore
from libraries.RecipeConverters import simplybackpacks
from libraries.RecipeConverters import smelting
from libraries.RecipeConverters import sophisticatedbackpacks
from libraries.RecipeConverters import sophisticatedcore
from libraries.RecipeConverters import sophisticatedstorage
from libraries.RecipeConverters import statues
from libraries.RecipeConverters import supplementaries
from libraries.RecipeConverters import theOneProbe
from libraries.RecipeConverters import thermal
from libraries.RecipeConverters import tinkersConstruct
from libraries.RecipeConverters import titanium
from libraries.RecipeConverters import twilightforest
from libraries.RecipeConverters import usefulbackpacks
from libraries.RecipeConverters import uteamcore
from libraries.RecipeConverters import woot
from libraries.RecipeConverters import wormhole
from libraries.RecipeConverters import xreliquary
import libraries.utils as utils


class converterEntry:
    def __init__(self, name, fnc):
        self.name = name
        self.fnc = fnc


converters: list[converterEntry] = [
    converterEntry("ad", adAstra.ConvertAdAstraRecipe),
    converterEntry("appliedenergistics2", appliedEnergistics2.ConvertAE2Recipe),
    converterEntry("ae2", appliedEnergistics2.ConvertAE2Recipe),
    converterEntry("ars", arsNouveau.ConvertArsNouVeauRecipe),
    converterEntry("astralsorcery",  astralSorcery.ConvertAstralSorceryRecipe),
    converterEntry("bloodmagic", bloodMagic.ConvertBloodMagicRecipe),
    converterEntry("bookshelf", bookshelf.ConvertBookshelfRecipe),
    converterEntry("botania", botania.ConvertBotaniaRecipe),
    converterEntry("botanypots", botanyPots.ConvertBotanyPotsRecipe),
    converterEntry("buildinggadgets", buildingGadgets.ConvertBuildingGadgetsRecipe),
    converterEntry("car", car.ConvertCarRecipe),
    converterEntry("cataclysm", cataclysm.ConvertCataclysmRecipe),
    converterEntry("chiselsandbits", chisselAndBits.ConvertChisselAndBitsRecipe),
    converterEntry("computercraft", computercraft.ConvertComputercraftRecipe),
    converterEntry("crafting", crafting.ConvertCraftingRecipe),
    converterEntry("create", create.ConvertCreateRecipe),
    converterEntry("cyclic", cyclic.ConvertCyclicRecipe),
    converterEntry("eccentrictome", eccentrictome.ConvertEccentrictomeRecipe),
    converterEntry("emojiful", emojiful.ConvertEmojifulRecipe),
    converterEntry("enderchests", enderchests.ConvertEnderChestsRecipe),
    converterEntry("enderio", enderio.ConvertEnderioRecipe),
    converterEntry("enderstorage", enderStorage.ConvertEnderstorageRecipe),
    converterEntry("endertanks", endertanks.ConvertEndertanksRecipe),
    converterEntry("energizedpower", energizedpower.ConvertEnergizedPowerRecipe),
    converterEntry("everlastingabilities", everlastingabilities.ConvertEverlastingAbilitiesRecipe),
    converterEntry("fluxnetworks", fluxNetworks.ConvertFluxNetworksRecipe),
    converterEntry("immersiveengineering", immersiveEngineering.ConvertImmersiveEngineeringRecipe),
    converterEntry("industrialforegoing", industrialForegoing.ConvertIndustrialForegoingRecipe),
    converterEntry("integrateddynamics", integratedDynamics.ConvertIntegratedDynamicsRecipe),
    converterEntry("ironfurnaces", ironfurnaces.ConvertIronFurnacesRecipe),
    converterEntry("laserio", laserio.ConvertLaserIoRecipe),
    converterEntry("mana-and-artifice", manaAndArtifice.ConvertManaAndArtificeRecipe),
    converterEntry("mantle", mantle.ConvertMantleRecipe),
    converterEntry("mcjtylib", mcjtylib.ConvertMcjtylibRecipe),
    converterEntry("mekanism", mekanism.ConvertMekanismRecipe),
    converterEntry("mekanismtools", None),
    converterEntry("minecolonies", mineColonies.ConvertMineColoniesRecipe),
    converterEntry("minecraft", minecraft.ConvertMinecraftRecipe),
    converterEntry("modulargolems", modulargolems.ConvertModularGolemsRecipe),
    converterEntry("modularrouters", modularRouters.ConvertModularRoutersRecipe),
    converterEntry("mowlib", mowlib.ConvertMowlibRecipe),
    converterEntry("mythicbotany", mythicBotany.ConvertMythicBotanyRecipe),
    converterEntry("naturesaura", naturesAura.ConvertNaturesAuraRecipe),
    converterEntry("occultism", occultism.ConvertOccultismRecipe),
    converterEntry("oreberriesreplanted", oreberriesreplanted.ConvertOreberriesReplantedRecipe),
    converterEntry("patchouli", patchouli.ConvertPatchouliRecipe),
    converterEntry("pedestals", pedestals.ConvertPedestalsRecipe),
    converterEntry("pipez", pipez.ConvertPipezRecipe),
    converterEntry("planttech2", planttech2.ConvertTemplateRecipe),
    converterEntry("pneumaticcraft", pneumaticCraft.ConvertPneumaticCraftRecipe),
    converterEntry("pocketstorage", pocketStorage.ConvertPocketStorageRecipe),
    converterEntry("portabletanks", portabletanks.ConvertPortableTanksRecipe),
    converterEntry("powah", powah.ConvertPowahRecipe),
    converterEntry("propellerhats", propellerhats.ConvertPropellorHatsRecipe),
    converterEntry("psi", psi.ConvertPsiRecipe),
    converterEntry("rats", rats.ConvertRatsRecipe),
    converterEntry("rechiseled", rechiseled.ConvertRechiseledRecipe),
    converterEntry("recipe", Recipe.ConvertRecipeRecipe),
    converterEntry("refinedstorage", refinedstorage.ConvertRefinedStorageRecipe),
    converterEntry("rftoolsdim", rftoolsDim.ConvertRftoolsDimRecipe),
    converterEntry("rftoolsutility", None),
    converterEntry("rootsclassic", rootsclassic.ConvertRootsClassicRecipe),
    converterEntry("sewingkit", sewingkit.ConvertSewingKitRecipe),
    converterEntry("sfm", sfm.ConvertSFMRecipe),
    converterEntry("shetiphiancore", shetiphiancore.ConvertshetiphiancoreRecipe),
    converterEntry("simplybackpacks", simplybackpacks.ConvertSimplyBackpacksRecipe),
    converterEntry("smelting", smelting.ConvertSmeltingRecipe),
    converterEntry("sophisticatedbackpacks", sophisticatedbackpacks.ConvertSophisticatedBackpacksRecipe),
    converterEntry("sophisticatedcore", sophisticatedcore.ConvertSophisticatedCoreRecipe),
    converterEntry("sophisticatedstorage", sophisticatedstorage.ConvertSophisticatedStorageRecipe),
    converterEntry("statues", statues.ConvertStatuesRecipe),
    converterEntry("supplementaries", supplementaries.ConvertSupplementariesRecipe),
    converterEntry("theoneprobe", theOneProbe.ConvertTheOneProbeRecipe),
    converterEntry("thermal", thermal.ConvertThermalRecipe),
    converterEntry("tconstruct", tinkersConstruct.ConvertTinkersConstructRecipe),
    converterEntry("titanium", titanium.ConvertTitaniumRecipe),
    converterEntry("twilightforest", twilightforest.ConvertTwilightForestRecipe),
    converterEntry("usefulbackpacks", usefulbackpacks.ConvertUsefulBackpacksRecipe),
    converterEntry("uteamcore", uteamcore.ConvertUteamCoreRecipe),
    converterEntry("woot", woot.ConvertWootRecipe),
    converterEntry("wormhole", wormhole.ConvertWormholeRecipe),
    converterEntry("xreliquary", xreliquary.ConvertXreliquaryRecipe),
]


def ConvertRecipe(recipe):
    modName = recipe["type"].split(":")
    modName = modName[0].lower()
    modName = modName.split("_")
    modName = modName[0]

    for entry in converters:
        if entry.name == modName:
            if entry.fnc == None:
                return None
            else:
                return entry.fnc(recipe)
    utils.Print("no converter for mod " + modName +
                " recipeType: " + recipe["type"])
    return None
