import json
import os

#holds all items and tags
class itemAndTagDb:
    def __init__(self):
        self.tagDb = {}
        self.itemDb = []
    #adds an item to the database without adding tags
    def AddItemWithoutTagsToDb(self, itemName):
        if itemName not in self.itemDb:
            self.itemDb.append(itemName)
    #adds an item to the database with apropriate tags
    def AddItemsToDb(self, tagName, itemsForTag):
        if tagName not in self.tagDb:
            self.tagDb[tagName] = []
        for item in itemsForTag:
            if item not in self.tagDb[tagName]:
                if "#" in item:
                    tName = item.replace("#", "")
                    if tName not in self.tagDb: # # means the item itself is a tag so we will create an empty entry for it
                        self.tagDb[tName] = []
                else: 
                    self.tagDb[tagName].append(item)
                    if type(item) is not dict:
                        itemAndTagDb.AddItemWithoutTagsToDb(self, item)
    #get entire database including what items belong to what tag
    def GetDb(self):
        return self.tagDb, self.itemDb
    
    #get just the tags in database as a lookup table (dict)
    def GetTagsInDb(self):
        tags = []
        for tag in self.tagDb:
            if tag not in tags:
                tags.append(tag)
        return tags

    #get just the items in the db as a lookup table (dict)
    def GetItemsInDb(self):
        return self.itemDb