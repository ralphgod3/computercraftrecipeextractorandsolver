## Information and use  
This directory holds programs that I use for generating an item and tag database for use in my programs.  
When trying to run any script in this directory it is heavily advised to increase computer disk size to prevent issues,  
the file for this is located in WORLDSAVE/serverconfig/computercraft.toml  

### generateItemDB.lua  
Generate an item and tag database based on a file placed in the same directory named items.json.  
items.json can be aquired by running the python scripts in [this](https://gitlab.com/ralphgod3/computercraftrecipeextractorandsolver) repo.  
it also needs a chest and turtle attached to itself with the chest being autofilled with the item it spawns 2 blocks above itself.  
pictured below is a working setup.  
For this the turtle just needs to throw any item in its inventory away into something that destroys that item (lava or cactus).  
The turtle is running the dropper.lua code but anything that will periodically drop the items will work.  
![setup](itemDbsetup.png)
#### Note  
Spawning the items may take a while depending on your amount of mods, I suggest turning it on and grabbing something to eat or watch whilst it is working.  

#### output
This program will output multiple files for use by other scripts.  
1. The item database which is split over 2 files and is put by default in cache/itemDatabase/ Items| Tags .json  
1. failedItems.json a file that contains all items that it failed to spawn in.  
1. nbtMappings.json a file that contains mappings from unhashed nbt data to its hashed format used by computercraft.  

### filterCraftableRecipesAndConvertNbtTag.lua  
Converts the convertedRecipes.json file output by [this](https://gitlab.com/ralphgod3/computercraftrecipeextractorandsolver) set of scripts into a recipes.json file.  
the recipes.json file no longer contains the nbtUnhashed tag from convertedRecipes.json and it is replaced by the actual nbt hash as used by computercraft.  
It also removes any recipes that use items that could not be spawned in by generateItemDB.lua.  
  
For this script to work it needs:  
1. convertedRecipes.json (can be obtained from [this](https://gitlab.com/ralphgod3/computercraftrecipeextractorandsolver))  
1. failedItems.json is output by generateItemDB.lua  
1. nbtMappings.json is output by generateItemDB.lua  
  
#### Note  
Since the terminal will be spammed with messages quite abit and you may miss messages it will also output a log file at converterLogs.txt  

### using recipes.json  
As you might figure recipes.json is usually bigger than a computer can hold on its disk.  
I suggest putting it on pastebin or git and making the computer load it into ram on startup.  


## boring stuff
### Other files
As you may notice the scripts may download some files of git, if the files it downloads are broken create an issue.  
The files it downloads are libraries, should you want to use these I suggest rehosting them somewhere since the repo it downloads from may change substantially without warning.  

### license
Do not say you made the script.
I would also like it that if you make any changes that could be useful that you create a merge request since other people might also find that useful.  
I do not care what other stuff you do with the files / scripts.